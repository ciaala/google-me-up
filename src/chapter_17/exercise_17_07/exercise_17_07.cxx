#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}


class NameCombiner {
    unordered_map<string, string> synonymMap;
    unordered_map<string, uint32_t> counters;

    string &getParent(string &candidate) {
        string &found = candidate;

        while (synonymMap.count(found) > 0) {
            auto temp = synonymMap[found];
            if (found.compare(temp) == 0) {
                return found;
            }
            found = temp;
        }
        return found;
    }

public:
    vector<pair<string, uint32_t>> combine(
            vector<pair<string, uint32_t>> &frequencies,
            vector<pair<string, string>> &synonyms) {

        synonymMap.clear();

        for (pair<string, string> couple : synonyms) {
            cout << couple << endl;
            string *mainName = &couple.second;
            string *synonym = &couple.first;

            // The first element is already part of the system
            // we switch relation
            if (synonymMap.count(*synonym) > 0) {
                mainName = &couple.first;
                synonym = &couple.second;
            }

            auto parent = getParent(*mainName);
            synonymMap[*synonym] = parent;
            if (parent == *mainName) {
                counters[parent] = 0;
                synonymMap[parent] = parent;
            }
        }
        for (pair<string, uint32_t> frequency: frequencies) {
            auto parent = getParent(frequency.first);
            counters[parent] += frequency.second;
        }
        vector<pair<string, uint32_t>> result;
        for (pair<string, uint32_t> frequency: counters) {
            result.push_back(frequency);
        }
        return result;
    }
};

int main(int argc, char **argv) {
    NameCombiner combiner;
    vector<pair<string, uint32_t >> frequencies = {{"John",        15u},
                                                   {"Jon",         12u},
                                                   {"Chris",       13u},
                                                   {"Kris",        4u},
                                                   {"Christopher", 19u}};

    vector<pair<string, string >> synonyms = {{"Jon",   "John"},
                                              {"John",  "Johnny"},
                                              {"Chris", "Kris"},
                                              {"Chris", "Christopher"},
                                             };
    auto result = combiner.combine(frequencies, synonyms);
    cout << result << endl;
}