#include <list>
#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include "../../extra/avl_tree.hxx"
using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}
struct MedianElement {
  uint32_t value = 0;
  uint32_t count = 0;
  explicit MedianElement(uint32_t value) {
    this->value = value;
  }
};
class ContinuousMedian {
  AVLTree<uint32_t> values;
  unordered_map<uint32_t, MedianElement *> directAccess;
  MedianElement *current = nullptr;
  uint32_t left = 0;
  uint32_t right = 0;
 public:
  uint32_t median(uint32_t newValue) {
    MedianElement *element;
    // Track the element
    if (directAccess.count(newValue) == 0) {
      element = new MedianElement(newValue);
      values.insert(newValue);
      values.print(cout);
      directAccess[newValue] = element;
    } else {
      element = directAccess.at(newValue);
    }
    element->count++;
    if (current == nullptr) {
      current = element;
      return element->value;
    }
    // Update left or right
    if (element->value < current->value) {
      left++;
    } else if (element->value > current->value) {
      right++;
    }

    // Update the current
    if (left > right + current->count) {
      right += current->count;
      current = directAccess[values.getPrevious(current->value)];
      left -= current->count;
    } else if (right > left + current->count) {
      left += current->count;
      current = directAccess[values.getNext(current->value)];
      right -= current->count;
    }
    return current->value;
  }
};

static const int COUNTER = 32;
static const int RANGE = 10;

int main(int argc, char **argv) {
  ContinuousMedian tool;
  Dice dice(1234);
  for (auto i = 1u; i <= COUNTER; i++) {
    auto nextValue = dice.next(0, 100*RANGE);
    const auto median = tool.median(nextValue);
    cout << i << "] " << nextValue << " -> " << median << endl;
  }
  return 0;
}