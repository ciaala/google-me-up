//
// Created by Francesco Fiduccia on 2019-01-20.
//
#include <iostream>

using namespace std;

u_int32_t
addWithoutPlus(uint32_t a, uint32_t b) {
    uint32_t aa = a;
    uint32_t bb = b;
    while (bb > 0) {
        uint32_t op2 = aa & bb;
        aa = aa ^ bb;
        if (op2 & (1 << 31)) {
            cout << "overflow " << a << '+' << b;
            exit(-1);
        }
        bb = op2 << 1;
    }
    return aa;
}

void
check(uint32_t a, uint32_t b) {
    if (a + b != addWithoutPlus(a, b)) {
        cout << "!!! ";
    }
    cout << a << " + " << b << " = " << addWithoutPlus(a, b) << endl;
}

int main(int argc, char **argv) {
    check(1, 2);
    check(2, 2);
    check(0, 0);
    check(123, 13);
    check(102, 10);
    check(INT8_MAX, 1);
}