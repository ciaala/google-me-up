#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <list>
#include <algorithm>
#include <iterator>

using namespace std;

class Dice {
    std::default_random_engine generator;
    unsigned int seed;
public:
    Dice() : Dice((unsigned int) clock()) {

    }

    Dice(unsigned int seed) : seed(seed), generator(seed) {
        cout << "Seed: " << seed << endl;
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

vector<string> tokenization(const string &text) {
    istringstream iss(text);
    vector<string> results = {
            istream_iterator<string>(iss),
            istream_iterator<string>()
    };
    return results;
}

class hash_pair_string_string {
    const hash<string> ah = hash<string>();
public:
    size_t operator()(const pair<string, string> &key) const {
        auto seed = ah(key.first);
        return ah(key.first) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    }
};

class WordDistance {

    unordered_map<pair<string, string>, uint32_t, hash_pair_string_string> cache;


public:
    WordDistance(vector<string> &text) {
        buildCache(text);
    }

    void buildCache(vector<string> &text) {
        list<string *> wordSeen;
        for (string &word: text) {
            transform(word.begin(), word.end(), word.begin(), ::tolower);

            uint32_t dist = 0u;
            for (auto it = wordSeen.rbegin(); it != wordSeen.rend(); it++) {
                if (**it == word) {
                    wordSeen.erase(it.base());
                    break;
                } else {
                    auto key = makeKey(word, **it);
                    if (cache.count(key) == 0 || cache[key] > dist) {
                        cache[key] = dist;
                    }
                }
                dist++;
            }
            wordSeen.emplace_back(&word);
        }
    }

    pair<string, string> makeKey(string &word1, string &word2) {
        if (word1 > word2) {
            return {word2, word1};
        } else {
            return {word1, word2};
        }
    }

    uint32_t getDistance(string word1, string word2) {
        transform(word1.begin(), word1.end(), word1.begin(), ::tolower);
        transform(word2.begin(), word2.end(), word2.begin(), ::tolower);

        if (word1 == word2) {
            return 0;
        }
        auto key = makeKey(word1, word2);
        if (cache.count(key) == 0) {
            return UINT32_MAX;
        }
        return cache[key];
    }
};

int main(int argc, char **argv) {
    string text = "Dynamite with a laser beam\n"
                  "Guaranteed to blow your mind\n"
                  "Anytime\n"
                  "\n"
                  "Recommended at the price\n"
                  "Insatiable an appetite\n"
                  "wanna try?\n"
                  "\n"
                  "To avoid complications\n"
                  "She never kept the same address\n"
                  "In conversation she spoke just like a baroness\n"
                  "Met a man from China went down to Geisha Minah\n"
                  "Then again incidentally if you're that way inclined (she's a\n"
                  "killer queen)\n"
                  "\n"
                  "Perfume came naturally from Paris (naturally)\n"
                  "For cars she couldn't care less\n"
                  "Fastidious and precise\n"
                  "\n"
                  "She's a killer queen, gunpowder, gelatine\n"
                  "Dynamite with a laser beam\n"
                  "Guaranteed to blow your mind\n"
                  "Anytime\n"
                  "\n"
                  "Drop of a hat she's as willing as\n"
                  "playful as a pussy cat\n"
                  "Then momentarily out of action\n"
                  "Temporarily out of gas\n"
                  "To absolutely drive you wild - wild\n"
                  "She's out to get you\n"
                  "\n"
                  "She's a killer queen, gunpowder, gelatine\n"
                  "Dynamite with a laser beam\n"
                  "Guaranteed to blow your mind\n"
                  "Anytime\n"
                  "\n"
                  "Recommended at the price\n"
                  "Insatiable an appetite\n"
                  "wanna try?\n"
                  "\n"
                  "You wanna try\n"
                  "\n"
                  "https://www.letssingit.com/queen-lyrics-killer-queen-5zjr322\n"
                  "LetsSingIt - The Internet Lyrics Database";
    vector<string> wordSequence = tokenization(text);
    WordDistance wordDistance(wordSequence);
    cout << wordDistance.getDistance("with", "a") << endl;
    cout << wordDistance.getDistance("Database", "Internet") << endl;

}