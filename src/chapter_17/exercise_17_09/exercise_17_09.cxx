#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <list>

using namespace std;

class Dice {
    std::default_random_engine generator;
    unsigned int seed;
public:
    Dice() : Dice((unsigned int) clock()){

    }
    Dice(unsigned int seed) : seed(seed), generator(seed) {
        cout<< "Seed: " << seed << endl;
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

class KthGenerator {
    list <uint32_t> history;

    list<uint32_t>::iterator _3it;
    list<uint32_t>::iterator _5it;
    list<uint32_t>::iterator _7it;

    uint32_t _3th;
    uint32_t _5th;
    uint32_t _7th;

    bool update(uint32_t value, uint32_t &_XthValue, list<uint32_t>::iterator &_Xit, uint32_t _X) {
        if (value == _XthValue) {
            _Xit++;
            _XthValue = (*_Xit) * _X;
            return true;
        }
        return false;
    }

public:
    KthGenerator() {
        history.emplace_back(1);
        _3it = history.begin();
        _5it = history.begin();
        _7it = history.begin();

        _3th = *_3it * 3;
        _5th = *_5it * 5;
        _7th = *_7it * 7;

    }


    uint32_t next_value() {
        uint32_t minimum = min({_3th, _5th, _7th});
        history.emplace_back(minimum);
        update(minimum, _3th, _3it, 3);
        update(minimum, _5th, _5it, 5);
        if (update(minimum, _7th, _7it, 7)) {
            history.pop_front();
        }
        return minimum;
    }

    void print_stat() {
        cout << *_7it << ' ' << *_5it << ' ' << * _3it << endl;
    }
};

int main(int argc, char **argv) {
    KthGenerator generator;
    for (auto i = 0u; i < 100; i++) {

        cout << setw(3) << i << ' ' << setw(9) << generator.next_value() << endl;
    }
    generator.print_stat();
    return 0;
}