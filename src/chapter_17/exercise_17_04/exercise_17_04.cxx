#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T, size_t S>
ostream &operator<<(ostream &out, array<T, S> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T>
ostream &operator<<(ostream &out, const pair<T, T> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

template<typename T>
class FindMissingNumber {

private:
    int getBit(vector<T> &numbers, size_t i, size_t bit) {
        return numbers[i] & (1u << bit);
    }

public:
    size_t find(vector<T> &numbers, size_t bit = 0, T solution = 0) {
        vector<T> odds;
        vector<T> even;

        for (size_t i = 0u; i < numbers.size(); ++i) {
            if (getBit(numbers, i, bit) == 0) {
                even.emplace_back(numbers[i]);
            } else {
                odds.emplace_back(numbers[i]);
            }
        }
        vector<T> &candidate = odds;
        if (odds.size() > even.size()) {
            candidate = even;
        } else {
            solution |= (1u << bit);
        }
        cout << solution << '|' << bit << endl;
        if (candidate.empty()) {
            return solution;
        } else {
            return find(candidate, bit + 1, solution);
        }
    }

};

int main(int argc, char **argv) {
    const size_t N = 512 * 512;
    vector<size_t> numbers(N - 1);
    Dice dice;
    size_t skip = dice.next(0, N - 1);

    for (auto i = 0u; i < N; ++i) {
        if (i != skip) {
            numbers[i > skip ? i - 1 : i] = i;
        }
    }
    FindMissingNumber<size_t> finder;
    cout << '{' << skip << '}' << numbers << endl;
    cout << finder.find(numbers) << endl;

}