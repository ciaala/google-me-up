#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}

class WordTransformer {

  const unordered_set<string> dict;
 public:
  WordTransformer(std::vector<string> &dictionary) :
      dict(dictionary.begin(), dictionary.end()) {
  }

  const std::vector<string> transform(const string &source, const string &target) {

    unordered_map<string, std::vector<string>> cacheSource;
    unordered_map<string, std::vector<string>> cacheTarget;

    std::queue<string> workAlpha;
    std::queue<string> workBeta;
    workAlpha.push(source);
    workBeta.push(target);
    cacheSource[source] = {source};
    cacheTarget[target] = {target};
    while (!workAlpha.empty() && !workBeta.empty()) {
      if (!workAlpha.empty()) {
        cout << "source: " << workAlpha.front() << endl;
        const auto result = exploreWorld(workAlpha, cacheSource, cacheTarget, false);
        if (!result.empty()) return result;
      }
      if (!workBeta.empty()) {
        cout << "target: " << workBeta.front() << endl;
        const auto result = exploreWorld(workBeta, cacheTarget, cacheSource, true);
        if (!result.empty()) return result;
      }

    }
    return {};
  }
 private:
  const std::vector<string> exploreWorld(
      std::queue<string> &workQueue,
      unordered_map<string, std::vector<string>> &cacheSource,
      unordered_map<string, std::vector<string>> &cacheTarget,
      bool reverse) {
    string alpha = workQueue.front();
    workQueue.pop();

    if (cacheTarget.count(alpha) > 0) {
      return makeResultVector(cacheSource[alpha], cacheTarget[alpha], reverse);
    } else {
      auto result = updateWorkQueue(alpha, workQueue, cacheSource, cacheTarget, reverse);
      return result;
    }
  }

  const vector<string> makeResultVector(const vector<string> &source,
                                        const vector<string> &target,
                                        bool reverse) const {
    vector<string> result;
    result.reserve(source.size() + target.size());
    if (!reverse) {
      result.insert(result.begin(), source.begin(), source.end());
      result.insert(result.end(), target.rbegin(), target.rend());
    } else {
      result.insert(result.begin(), target.begin(), target.end());
      result.insert(result.end(), source.rbegin(), source.rend());
    }
    return result;
  }

  vector<string> updateWorkQueue(const string word,
                                 std::queue<string> &workQueue,
                                 unordered_map<string, std::vector<string>> &cacheSource,
                                 unordered_map<string, std::vector<string>> &cacheTarget,
                                 bool reverse) {

    std::vector<string> sequence = cacheSource.at(word);
    for (auto i = 0; i < word.size(); ++i) {
      for (auto j = 'A'; j != 'Z'; ++j) {
        string candidate = word;
        candidate[i] = j;
        if (dict.count(candidate) > 0) {
          if (cacheTarget.count(candidate) > 0) {
            return makeResultVector(sequence, cacheTarget[candidate], reverse);
          } else if (cacheSource.count(candidate) == 0) {
            workQueue.push(candidate);
            std::vector<string> sequenceCopy = sequence;
            sequenceCopy.push_back(candidate);
            cacheSource[candidate] = sequenceCopy;
          }
        }
      }
    }
    return {};
  }
};

int main(int argc, char **argv) {
  std::vector<string> dictionary = {"DAMP", "LAMP", "LIMP", "LIME", "LIKE",
                                    "ABC", "ABF", "ACF", "ZCF", "ZXF"};
  WordTransformer transformer(dictionary);
  {
    auto result = transformer.transform("DAMP", "LIKE");
    cout << result << endl;
  }
  {
    auto result = transformer.transform("ABC", "ZXF");
    cout << result << endl;
  }
  return 0;
}