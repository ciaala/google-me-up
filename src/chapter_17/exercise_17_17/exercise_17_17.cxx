#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <list>
#include "../../extra/trie.hxx"

using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}

template<uint32_t DICTIONARY_SIZE>
class MultipleSearch {
 private:
  trie root;
 public:
  MultipleSearch(std::array<string, DICTIONARY_SIZE> dictionary) {
    for (string &word: dictionary) {
      root.insert(word);
    }
  }

  unordered_map<string, vector<uint32_t>> matches(string &text) {
    std::unordered_map<std::string, std::vector<uint32_t>> result(0);
    std::list<const trie *> _scanners;
    std::list<const trie *> _scannersNext;
    std::list<const trie *> *following = &_scannersNext;
    std::list<const trie *> *current = &_scanners;
    current->push_back(&root);

    for (uint32_t index = 0; index < text.size(); ++index) {

      auto c = text[index];
      bool restart = false;
      for (auto it = current->begin();
           it != current->end();
           it++) {


        cout << "---------------" << endl;
        for_each(current->begin(), current->end(),
                 [](auto *item) { cout << *item << endl; });
        cout << endl;

        auto scanner = *it;
        if (scanner->contains(c)) {
          const trie *next = scanner->getNext(c);
          following->push_back(next);
          if (next->hasFinalWord()) {
            if (result.count(next->finalWord) == 0) {
              result[next->finalWord] = {index};
            } else {
              result[next->finalWord].push_back(index);
            }
            if (!restart) {
              following->push_back(&root);
              restart = true;
            }
          }
        }
      }
      if (!restart) {
        following->push_back(&root);
      }
      current->clear();
      auto tmp = current;
      current = following;
      following = tmp;
    }
    return result;
  }
};

int main(int argc, char **argv) {
  std::array<string, 6> T = {"is", "ppi", "hi",
                             "sis", "i", "ssipi"};
  string b = {"mississipi"};

  MultipleSearch<6> solver(T);
  auto solution = solver.matches(b);
  cout << "--------" << endl;
  for_each(solution.begin(), solution.end(), [](auto &item) { cout << item.first << ' ' << item.second << endl; });
}