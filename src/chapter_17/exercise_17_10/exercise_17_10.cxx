#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
    unsigned int seed;
public:
    Dice() : Dice((unsigned int) clock()) {

    }

    Dice(unsigned int seed) : seed(seed), generator(seed) {
        cout << "Seed: " << seed << endl;
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}


class IdentifyMajorityCandidate {
private:
    uint32_t getCandidate(const vector<uint32_t> &data) {

        auto isSelected = false;

        uint32_t candidate;
        uint32_t count = 0;
        for (auto i = 0; i < data.size(); i++) {
            if (!isSelected) {
                candidate = data[i];
                count = 1;
                isSelected = true;
            } else {
                count += data[i] == candidate ? 1 : -1;
                if (count == 0) {
                    isSelected = false;
                }
            }
        }
        return candidate;
    }

public:
    uint32_t findMajority(const vector<uint32_t> &data) {
        if (data.empty()) {
            return -1;
        }

        auto candidate = getCandidate(data);
        auto count = 0;
        for (auto i = 0; i < data.size(); i++) {
            count += data[i] == candidate ? 1 : -1;
        }
        if (count < 0) {
            return -1;
        }
        return candidate;
    }
};

void check(const vector<uint32_t> &data, uint32_t expected) {
    IdentifyMajorityCandidate identify;
    uint32_t actual = identify.findMajority(data);
    if (actual != expected) {
        cout << actual << " != " << expected;
    } else {
        cout << actual << " == " << expected;
    }
    cout << ' ' << data << endl;
}

int main(int argc, char **argv) {
    check({1, 1, 1, 1, 0, 0, 0, 0, 2}, -1);
    check({1, 1, 1, 1, 0, 0, 0, 0, 1}, 1);
    check({0, 0, 0, 0, 1, 1, 1, 1, 1}, 1);
    check({0, 0, 0, 0, 1, 1, 1, 1, 0}, 0);
    check({2, 0, 2, 0, 2, 1, 2, 1, 2}, 2);

}