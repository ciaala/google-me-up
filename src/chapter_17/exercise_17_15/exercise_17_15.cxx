#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <algorithm>
#include <functional>
#include "../../extra/trie.hxx"

using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}

class FindLongestWord {
 private:
  static vector<string> typeStrings;
  static std::function<bool(const string &, const string &)> sortShorterFirst;

 public:
  enum TYPE { FORWARD, BACKWARD, TRIE };

 public:
  string static typeToString(FindLongestWord::TYPE type) {
    return typeStrings[type];
  }
  string static find(std::vector<string> &words, TYPE type = TYPE::FORWARD) {

    if (words.empty()) {
      return "";
    }
    cout << words << endl;
    if (type == TYPE::TRIE) {
      return find_trie(words);
    }
    std::sort(words.begin(), words.end(), sortShorterFirst);
    auto finder = (type == TYPE::FORWARD) ? FindLongestWord::find_forward : FindLongestWord::find_backward;
    unordered_set<string> easyAccess(words.begin(), words.end());
    unordered_set<string> partialMatch;

    for (string word:  words) {
      cout << "trying: " << word << endl;
      easyAccess.erase(word);
      const unsigned long index = type == TYPE::FORWARD ? 0 : word.size();
      bool exist = finder(word, index, easyAccess, partialMatch);
      if (exist) {
        return word;
      } else {
        cout << "not good: " << word << endl;
      }
      // easyAccess.insert(word)
    }

    return "";
  }

 private:
  string static find_trie(std::vector<string> &words) {
    std::sort(words.begin(), words.end(),
              [](const string &s1, const string &s2) -> bool {
                int result = s1.size() - s2.size();
                return result != 0 ? result < 0 : s2.compare(s1);
              });
    trie master;
    string longest;
    for (auto &word : words) {
      cout << "word : " << word << endl;
      if (find_trie(word, 0, &master, &master)) {
        longest = word;
      }
      master.insert(word);
    }
    return longest;
  }
  bool static find_trie(const string &word, const unsigned index, const trie *current, const trie *master) {
    if (word.size() == index && current == master) {
      return true;
    }
    const auto c = word[index];
    if (current->contains(c)) {
      auto *next = current->getNext(c);
      if (!next->finalWord.empty()) {
        cout << "matching sequence: " << next->finalWord << endl;
        bool result = find_trie(word, index + 1, master, master);
        if (result) {
          return true;
        } else {
          cout << "unmatching: " << next->finalWord << endl;
        }
      }
      return find_trie(word, index + 1, next, master);
    } else {
      cout << "failing at '" << word.substr(0, index) << "_" << word[index] << '_'
           << (word.size() > index ? word.substr(index + 1) : " ") << endl;
      return false;
    }
  }

  bool static find_backward(const string &word,
                            const unsigned long end,
                            unordered_set<string> &easyAccess,
                            unordered_set<string> &partialMatch) {
    for (int start = 0; start < end; start++) {
      auto fragment = word.substr(start, end - start);
      if (easyAccess.count(fragment) > 0 || partialMatch.count(fragment) > 0) {
        auto segment = word.substr(0, end);
        cout << "  match: " << fragment << " (" << segment << ")" << endl;
        if (start != 0) {
          bool result = find_backward(word, start, easyAccess, partialMatch);
          if (result) {
            return true;
          } else {
            cout << "unmatch: " << fragment << endl;
          }
        } else {
          return true;
        }
      }
    }
    cout << "not matching: " << word.substr(0, end) << endl;
    return false;
  }
  bool static find_forward(const string &word,
                           const unsigned long start,
                           unordered_set<string> &easyAccess,
                           unordered_set<string> &partialMatch) {
    for (int end = word.size(); end > start; end--) {
      auto fragment = word.substr(start, end - start);
      if (easyAccess.count(fragment) > 0 || partialMatch.count(fragment) > 0) {
        auto segment = word.substr(0, end);
        cout << "  match: " << fragment << " (" << segment << ")" << endl;
        if (end != word.size()) {
          partialMatch.insert(segment);
          bool result = find_forward(word, end, easyAccess, partialMatch);
          if (result) {
            return true;
          } else {
            cout << "unmatch: " << fragment << endl;
          }
        } else {
          return true;
        }
      }
    }
    cout << "not matching: " << word.substr(start) << endl;
    return false;
  }
};
vector<string> FindLongestWord::typeStrings = {"FORWARD", "BACKWARD", "TRIE"};
std::function<bool(const std::string &, const std::string &)>
    FindLongestWord::sortShorterFirst = [](const std::string &s1, const std::string &s2) -> bool {
  int result = s1.size() - s2.size();
  return result != 0 ? result > 0 : s2.compare(s1);
};
void callSolver(vector<string> &words, FindLongestWord::TYPE type) {
  cout << "------------------------------------------------- " << FindLongestWord::typeToString(type) << endl;
  {
    string result = FindLongestWord::find(words, type);
    cout << "--> found: " << '\'' << result << '\'' << endl;
  }
}
int main(int argc, char **argv) {
  std::vector<string> words = {"abc",
                               "cde",
                               "veg",
                               "de",
                               "cdeabcveg1",
                               "abcdeabcveg1",
                               "cdeabcveg",
                               "abcve"};
  callSolver(words, FindLongestWord::FORWARD);
  callSolver(words, FindLongestWord::BACKWARD);
  callSolver(words, FindLongestWord::TRIE);
  return 0;
}
