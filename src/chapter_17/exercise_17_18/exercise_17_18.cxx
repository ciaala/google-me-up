#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}
class FindSequence {
 public:
  std::pair<uint32_t, uint32_t>
  findShortSequence(std::vector<uint32_t> &unique,
                    std::vector<uint32_t> &sequence) {
    std::pair<uint32_t, uint32_t> result = {0, UINT32_MAX};
    unordered_set<uint32_t> unorderedSet;

    unorderedSet.insert(unique.begin(), unique.end());

    std::pair<uint32_t, uint32_t> best = {0, UINT32_MAX};
    uint32_t counted = 0;
    unordered_map<uint32_t, uint32_t> counters;

    for (uint32_t index = 0; index < sequence.size(); ++index) {
      auto element = sequence[index];
      if (counters.count(element) > 0) {
        counters[element] += 1;
      } else {
        if (unorderedSet.count(element) > 0) {
          counted++;
          counters[element] = 1;
        }
      }
      if (counted == unique.size()) {
        result.second = index;
        for (auto start = result.first; start < result.second; start++) {
          const auto item = sequence[start];
          if (unorderedSet.count(item) == 0) {
            result.first++;
          } else if (counters[item] > 1) {
            counters[item] -= 1;
            result.first++;
          } else {
            break;
          }
        }
        if ((result.second - result.first) < (best.second - best.first)) {
          best = result;
        }
      }
    }
    return best;
  }
};

int main(int argc, char **argv) {
  std::vector<uint32_t> unique = {1, 5, 9};
  std::vector<uint32_t> sequence = {7, 5, 9, 0, 2, 1, 3, 5, 7, 9, 1, 1, 5, 8, 8, 9, 7};
  FindSequence findSequence;
  auto solution = findSequence.findShortSequence(unique, sequence);
  cout << solution << endl;
  for (int i = 0; i < sequence.size(); i++) {
    auto &item = sequence[i];
    cout
        << (i == solution.first ? "_" : "")
        << item
        << (i == solution.second ? "_" : "")
        << (i != sequence.size()-1 ? ',' : ' ');
  }
}