#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <algorithm>

using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p);

template<typename T, size_t SIZE>
ostream &operator<<(ostream &out, const std::array<T, SIZE> &result) {
  for (auto v : result) {
    out << v;
  }
  return out;
}

template<typename T, size_t SIZE1, size_t SIZE2>
ostream &operator<<(ostream &out, const std::array<std::array<T, SIZE1>, SIZE2> &result) {
  for (auto v : result) {
    out << v << endl;
  }
  return out;
}

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](const auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}
template<size_t SIZE>
class FindSubSquare {
 private:
  const bool debug = false;
  std::array<std::array<std::pair<uint32_t, uint32_t>, SIZE>, SIZE> partialData = {std::pair<uint32_t, uint32_t>(0, 0)};

  std::pair<uint32_t, uint32_t>

  getPartialData(const std::array<std::array<bool, SIZE>, SIZE> &data, const uint32_t x, const uint32_t y) {
    if (x >= SIZE || y >= SIZE) {
      return {0, 0};
    }
    if (data[x][y] == false) {
      partialData[x][y] = {0, 0};
    } else if (partialData[x][y].first == 0 && partialData[x][y].second == 0) {
      partialData[x][y].first = 1 + getPartialData(data, x + 1, y).first;
      partialData[x][y].second = 1 + getPartialData(data, x, y + 1).second;
    }
    return partialData[x][y];
  }
  void calculatePartial(const std::array<std::array<bool, SIZE>, SIZE> &data) {
    for (uint32_t x = 0; x < SIZE; ++x) {
      for (uint32_t y = 0; y < SIZE; ++y) {
        getPartialData(data, x, y);
      }
    }
    cout << partialData << endl;
  }

 public:
  explicit FindSubSquare(const bool debug) : debug(debug) {

  }
  bool checkSquareWithPartial(const uint32_t x,
                              const uint32_t y,
                              const uint32_t squareSize) {
    if (debug) {
      cout << "-> " << squareSize << endl;
      cout << '\t' << x << ',' << y << ' ' << endl;
    }
    const auto partialTopLeft = partialData[x][y];
    const auto partialTopRight = partialData[x + squareSize - 1][y];
    const auto partialBottomLeft = partialData[x][y + squareSize - 1];
    if (debug) {
      cout << "\ttl:" << partialTopLeft;
      cout << "\tbl:" << partialBottomLeft;
      cout << "\ttr:" << partialTopRight << endl;
    }
    return partialTopLeft.first >= squareSize && partialTopLeft.second >= squareSize
        && partialBottomLeft.first >= squareSize && partialTopRight.second >= squareSize;
  }

  std::vector<std::pair<uint32_t, uint32_t>>
  findWithPartial(const std::array<std::array<bool, SIZE>, SIZE> &data) {
    this->calculatePartial(data);
    for (uint32_t size = 0; size < SIZE; ++size) {
      uint32_t squareSize = SIZE - size;
      if (debug) {
        cout << endl << "->" << squareSize << endl;
      }
      for (uint32_t x = 0; x <= size; ++x) {
        for (uint32_t y = 0; y <= size; ++y) {
          auto found = this->checkSquareWithPartial(x, y, squareSize);
          if (found) {
            std::vector<std::pair<uint32_t, uint32_t>>
                result = {{x, y},
                          {x, y + squareSize - 1},
                          {x + squareSize - 1, y},
                          {x + squareSize - 1, y + squareSize - 1}};
            return result;
          }
        }
      }
    }
    return {{}, {}, {}, {},};
  }

  std::vector<std::pair<uint32_t, uint32_t>>

  find(const std::array<std::array<bool, SIZE>, SIZE> &data) {

    for (uint32_t size = 0; size < SIZE; ++size) {
      uint32_t squareSize = SIZE - size;
      if (debug) {
        cout << endl << "->" << squareSize << endl;
      }
      for (uint32_t x = 0; x <= size; ++x) {
        for (uint32_t y = 0; y <= size; ++y) {
          auto found = this->checkSquare(data, x, y, squareSize);
          if (found) {
            std::vector<std::pair<uint32_t, uint32_t>>
                result = {{x, y},
                          {x, y + squareSize - 1},
                          {x + squareSize - 1, y},
                          {x + squareSize - 1, y + squareSize - 1}};
            return result;
          }
        }
      }
    }
    return {{}, {}, {}, {},};
  }

 private:
  int getCell(array<array<bool, SIZE>, SIZE> &data, const uint32_t i, const uint32_t j) const {
    if (debug) {
      cout << i << '-' << j << ',';
    }
    auto result = data[j][i] != '0';
    if (result) {
      data[j][i] = '!';
    } else {
      data[j][i] = '?';
    }
    return result;
  }
  bool checkSquare(std::array<std::array<bool, SIZE>, SIZE> data,
                   const uint32_t x,
                   const uint32_t y,
                   const uint32_t squareSize) {
    if (debug) {
      cout << '\t' << x << ' ' << y << ' ' << endl;
    }
    for (auto i = x; i < x + squareSize; ++i) {
      if (getCell(data, i, y) == 0) {
        if (debug) {
          cout << endl << endl << data << endl;
        }
        return false;
      }
    }
    cout << endl;
    for (auto i = x; i < x + squareSize; ++i) {
      if (getCell(data, i, y + squareSize - 1) == 0) {
        if (debug) {
          cout << endl << endl << data << endl;
        }
        return false;
      }
    }
    for (auto j = y + 1; j < y + squareSize; ++j) {
      if (getCell(data, x, j) == 0) {
        if (debug) {
          cout << endl << endl << data << endl;
        }
        return false;
      }
    }
    cout << endl;
    for (auto j = y + 1; j < y + squareSize - 1; ++j) {
      if (getCell(data, squareSize + x - 1, j) == 0) {
        if (debug) {
          cout << endl << endl << data << endl;
        }
        return false;
      }
    }
    cout << endl << data << endl;
    return true;
  }
};

int main(int argc, char **argv) {
  Dice dice(32875);
  const auto SIZE = 512;
  std::array<std::array<bool, SIZE>, SIZE> data = {false};
  for (int i = 0; i < SIZE; ++i) {
    for (int j = 0; j < SIZE; ++j) {
      data[i][j] = dice.next(0, 1);
    }
  }
  /*
  data[1][1] = '1';
  data[1][2] = '1';
  data[2][1] = '1';
  data[2][2] = '1';
  */
  FindSubSquare<SIZE> subSquare(false);
  if (false) {
    std::vector<std::pair<uint32_t, uint32_t>> result = subSquare.find(data);
    std::cout << endl << endl;
    std::cout << data << endl;
    std::cout << result << endl;
  }
  auto result = subSquare.findWithPartial(data);
  std::cout << endl << endl;
  std::cout << data << endl;
  std::cout << result << endl;
  return 0;
}