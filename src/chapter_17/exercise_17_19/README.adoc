= 17.19: Missing two

Given a sequence of numbers from 0 to N find the missing element in O(N) time and O(1) space

What if there are 2 missing elements?
