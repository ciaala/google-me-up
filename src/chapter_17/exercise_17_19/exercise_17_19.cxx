#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <ctime>
#include <algorithm>
using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) time(nullptr)) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}
class FindMissingElement {
 public:
  static uint32_t findMissingElement(std::vector<uint32_t> &data,
                                     uint32_t start,
                                     uint32_t end) {
    uint32_t sum = (uint32_t) (((float) (start + end) / 2.0) * (float) (end - start + 1));
    cout << "expected: " << sum << endl;
    for (uint32_t i = 0; i < data.size(); ++i) {
      sum -= data[i];
    }
    return sum;
  }
};

class FindMissingElements {
  static const uint64_t LIMIT_MULTIPLICATION = UINT32_MAX / 2;
 public :
  static std::pair<uint32_t, uint32_t> findMissingElement(std::vector<uint32_t> &data,
                                                          uint32_t start,
                                                          uint32_t end) {

    auto sum = (uint64_t) (((double) (start + end) / 2.0) * (double) (end - start + 1));
    //  uint64_t quotient = 1;
    //  uint64_t remainder = 0;
    double mul = 1;
    double j = end;
    cout << "expected: " << sum << endl;
    divideMultiplicationAccumulator(start, mul, j);
    for (uint32_t i = 0; i < data.size(); ++i) {
      sum -= data[i];
      // cout << i << ' ' << data[i] << endl;
      mul /= data[i];
      divideMultiplicationAccumulator(start, mul, j);

    }
    divideMultiplicationAccumulator(start, mul, j);
    cout << "{mul: " << mul << ", sum: " << sum << '}' << endl;
    pair<uint32_t, uint32_t> solution;
    auto radix = (uint64_t) sqrt(sum * sum - 4 * mul);
    cout << "radix: " << radix << endl;
    solution.first = (radix > sum ? (sum + radix) : (sum - radix)) / 2;
    solution.second = sum - solution.first;
    return solution;
  }
  static void divideMultiplicationAccumulator(uint32_t start, double &mul, double &j) {
    while (j > start && (mul < LIMIT_MULTIPLICATION && j < LIMIT_MULTIPLICATION)) {
      mul *= j;
      j--;
    }
  }
};

void exampleMissing2Elements(const unsigned int SEED, Dice &dice, const int END, const int START) {// 2 elements
  cout << "------------------------------------" << endl;
  uint32_t missing1 = dice.next(START, END);
  uint32_t missing2 = dice.next(START, END);
  cout << "Missing: " << missing1 << ',' << missing2 << endl;
  vector<uint32_t> data;

  for (uint32_t i = START; i <= END; ++i) {
    if (i != missing1 && i != missing2) {
      data.emplace_back(i);
    }
  }
  shuffle(data.begin(), data.end(), default_random_engine(SEED));
  cout << data << endl;
  const auto missingElement = FindMissingElements::findMissingElement(data, START - 1, END);
  cout << missing1 << ' ' << missing2 << '=' << missingElement << endl;

}

void exampleMissingSingleElement(const unsigned int SEED, Dice &dice, const int END, const int START) {// 1 element
  cout << "------------------------------------" << endl;
  uint32_t missing = dice.next(START, END);
  cout << "Missing: " << missing << endl;

  vector<uint32_t> data;
  for (uint32_t i = START; i <= END; ++i) {
    if (i != missing) {
      data.emplace_back(i);
    }
  }
  shuffle(data.begin(), data.end(), default_random_engine(SEED));
  cout << data << endl;
  const auto missingElement = FindMissingElement::findMissingElement(data, START, END);
  cout << missing << '=' << missingElement << endl;

}

int main(int argc, char **argv) {
  static const unsigned SEED = time(nullptr);
  Dice dice(SEED);
  const auto END = 50;
  const auto START = 1;
  exampleMissingSingleElement(SEED, dice, END, START);
  exampleMissing2Elements(SEED, dice, END, START);

  return 0;
}