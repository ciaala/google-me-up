#include <vector>
#include <algorithm>
#include <iostream>
#include <random>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

vector<int> pickRandomly(Dice &dice, vector<int> &numbers, int count) {

    for (uint32_t i = 0; i < numbers.size(); ++i) {
        uint32_t k = dice.next(0, i);
        auto t = numbers[i];
        numbers[i] = numbers[k];
        numbers[k] = t;
    }
    vector<int> result(count);
    copy_n(numbers.begin(), count, result.begin());
    return result;
}

ostream &operator<<(ostream &out, vector<int> &data) {

    out << "[";
    for (int i : data) {
        out << i << ',';
    }
    out << "]";
    return out;
}

int main(int argc, char **argv) {
    Dice dice;
    vector<int> numbers(256);
    for (int i = 0; i < 256; ++i) {
        numbers[i] = i;
    }
    auto result = pickRandomly(dice, numbers, 8);
    cout << result << endl;

}