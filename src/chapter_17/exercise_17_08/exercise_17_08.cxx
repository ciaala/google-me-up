#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <functional>
#include <list>

using namespace std;

class Dice {
    std::default_random_engine generator;

    uint64_t seed;
public:
    Dice() : Dice((unsigned int) clock()){

    }
    Dice(unsigned int seed) : seed(seed), generator(seed) {
        cout<< "Seed: " << seed << endl;
    }
    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        auto value = distribution(generator);
        cout << value<<endl;
        return value;
    }
};


template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) {
        out << e;
        out << ' ';
    });
    out << ']';
    return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

class Person {
    static volatile uint32_t counter;
public:
    uint32_t id;
    uint32_t height;
    uint32_t weight;

    uint32_t tree_depth;

    unordered_set<Person *> children;

    Person(uint32_t height, uint32_t weight) : height(height), weight(weight), id(counter++), tree_depth(0) {

    }

    friend
    ostream &operator<<(ostream &out, const Person &person) {
        out << '{' << person.id << ':' << person.tree_depth << ':' << person.weight << ',' << person.height << "}[";
        bool hasPrinted = false;
        for (Person *child : person.children) {
            if (hasPrinted) {
                out << ',';
            } else {
                hasPrinted = true;
            }
            out << *child;
        }
        out << "]";
        return out;
    }
};

class DepthOStream {
    uint32_t space = 6;
    uint32_t depth = 1;
    ostream &out;
public:

    DepthOStream(ostream &out) : out(out) {}

    void print(Person *root) {
        out << '{' << root->id << ':' << root->tree_depth << ':' << root->weight << ',' << root->height << '}' << endl;
        unordered_set<uint32_t> ids;
        auto shouldExit = false;
        for (Person *person : root->children) {
            out << setfill('-') << setw(depth * space) << '-';
            depth++;
            print(person);

            depth--;
            if (ids.count(person->id) > 0) {
                shouldExit = true;
                cout << "duplicated: " << person->id;
            } else {
                ids.insert(person->id);
            }
        }
        if (shouldExit) {
            exit(255);
        }
    }
};

volatile uint32_t Person::counter = 0u;

class MakeTower {
    Person root{100, 100};
    vector<Person> &people;
private:
    void buildTree(Person *root, Person *current) {
        bool inserted = false;
        bool addAtThisLevel = false;
        // vector<Person *> explore = root->children;
        for (auto it = root->children.begin(); it != root->children.end(); ) {
            Person *child = *it;
            if (current->height < child->height && current->weight < child->weight) {
                inserted = true;
                buildTree(child, current);
                root->tree_depth = max(root->tree_depth, child->tree_depth+1);
            } else if (current->height > child->height && current->weight > child->weight) {
                cout << "deleting " << **it << endl;
                it = root->children.erase(it);
                addAtThisLevel = true;
                inserted = true;
                current->children.insert(child);
                current->tree_depth = max(current->tree_depth, child->tree_depth + 1);
                root->tree_depth = max(root->tree_depth, current->tree_depth + 1);
            }
            if (it != root->children.end()) {
                it++;
            }
        }
        if (addAtThisLevel) {
            root->children.insert(current);
        }
        if (!inserted) {
            // left branch in depth
            root->children.insert(current);
            current->tree_depth = 1;
            root->tree_depth = max(root->tree_depth, current->tree_depth + 1);
        }
    }

public:
    MakeTower(vector<Person> &people) : people(people) {
    }

    uint32_t findTallestTower() {
        DepthOStream out(cout);
        for (Person &person : people) {
            cout << "T -> " << person << endl;
            buildTree(&root, &person);
            out.print(&root);
            cout << setw(32) << setfill('-') << '-' << endl;
        }
        return root.tree_depth;
    }
};

void fillPeopleVector(vector<Person> &people, uint32_t size) {

    Dice dice;
    for (auto i = 0u; i < size; i++) {
        people.emplace_back(dice.next(0, size), dice.next(0, size));
    }
}

int main(int argc, char **argv) {
    vector<Person> people;

    MakeTower makeTower(people);
    /*people = {
            {1, 1},
            {1, 1},
            {2, 1},
            {1, 2},
            {2, 3},
            {4, 4},
            {2, 2}
    };*/
    fillPeopleVector(people, 30);
    cout << people << endl;
    cout << makeTower.findTallestTower() << endl;
}