#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {
  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
class BiNode;

template<typename T>
BiNode<T> *convertFromTreeToList(BiNode<T> *root) {
  queue<BiNode<T> *> memo;
  convertFromTreeToList<T>(root, &memo);
  return memo.front();
}

template<typename T>
void convertFromTreeToList(BiNode<T> *root, queue<BiNode<T> *> *memo) {
  if (root->left != nullptr) {
    convertFromTreeToList(root->left, memo);
  }
  if (!memo->empty()) {
    memo->back()->right = root;
    root->left = memo->back();
  }

  memo->push(root);
  if (root->right != nullptr) {
    convertFromTreeToList(root->right, memo);
  }
}

template<typename T>
class BiNode {
 public:
  BiNode *left;
  BiNode *right;
  T value;
  explicit BiNode(T value) : value(value), left(nullptr), right(nullptr) {}

  // template<typename Y>
  friend BiNode<T> *convertFromTreeToList<T>(BiNode<T> *root);

  //  template<typename Y>
  friend void convertFromTreeToList<T>(BiNode<T> *root, queue<BiNode<T> *> *memo);
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}

int main(int argc, char **argv) {
  auto *root = new BiNode<uint32_t>(8);
  root->left = new BiNode<uint32_t>(4);
  root->left->left = new BiNode<uint32_t>(2);
  root->left->left->left = new BiNode<uint32_t>(1);
  root->left->left->right = new BiNode<uint32_t>(3);
  root->left->right = new BiNode<uint32_t>(6);
  root->left->right->left = new BiNode<uint32_t>(5);
  root->left->right->right = new BiNode<uint32_t>(7);

  root->right = new BiNode<uint32_t>(12);
  root->right->left = new BiNode<uint32_t>(10);
  root->right->left->left = new BiNode<uint32_t>(9);
  root->right->left->right = new BiNode<uint32_t>(11);
  root->right->right = new BiNode<uint32_t>(14);
  root->right->right->left = new BiNode<uint32_t>(13);
  root->right->right->right = new BiNode<uint32_t>(15);
  BiNode<uint32_t> *head = convertFromTreeToList<uint32_t>(root);

  for (auto it = head; it != nullptr; it = it->right) {
    auto string = it->left == nullptr ? "" :
                  to_string(it->left->value);
    cout << " [" << string << ']' << it->value << ' ';
  }
  return 0;
}
