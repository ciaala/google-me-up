#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}
class CalculateVolumeOfWater {

 public:
  uint32_t calculate(const vector<uint32_t> &data) {
    const auto dataSize = data.size();
    vector<uint32_t> leftHeight(dataSize, 0);
    vector<uint32_t> rightHeight(dataSize, 0);
    vector<uint32_t> height(dataSize, 0);
    uint32_t maxHeight = 0;
    for (uint32_t i = 0; i < dataSize; ++i) {
      maxHeight = std::max(maxHeight, data[i]);
      leftHeight[i] = maxHeight;
    }
    maxHeight = 0;
    for (uint32_t i = 0; i < dataSize; ++i) {
      auto j = dataSize - i;
      maxHeight = std::max(maxHeight, data[j]);
      rightHeight[j] = maxHeight;
    }

    for (uint32_t i = 0; i < dataSize; ++i) {
      height[i] = std::min(leftHeight[i], rightHeight[i]);
      height[i] = height[i] > data[i]? height[i] - data[i] : 0;
    }
    return std::accumulate(height.begin(), height.end(), 0u, [](uint32_t &acc, uint32_t &value) { return acc + value; });
  }
};
int main(int argc, char **argv) {
  std::vector<uint32_t> data = {0, 0, 4, 0, 0, 6, 0, 0, 3, 0, 5, 0, 1, 0, 0, 0};
  CalculateVolumeOfWater water;

  auto result = water.calculate(data);
  cout << "result: " << result << endl;
  return 0;
}