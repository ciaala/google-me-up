#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <list>

using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}

vector<string> tokenization(const string &text) {
  istringstream iss(text);
  vector<string> results = {
      istream_iterator<string>(iss),
      istream_iterator<string>()
  };
  return results;
}

class Trine {
 public:
  unordered_map<char, Trine *> next;
  const char current;
  const bool isWord;

  explicit Trine(const char c, const bool isWord) : current(c), isWord(isWord) {}

  explicit Trine(const vector<string> &dictionary) : current(' '), isWord(false) {

    for (auto &word: dictionary) {
      Trine *temp = this;
      for (auto i = 0u; i < word.length(); i++) {
        char c = word[i];
        if (temp->next.count(c) > 0) {
          temp = temp->next[c];
        } else {
          auto aNew = new Trine(c, i == word.length() - 1);
          temp->next[c] = aNew;
          temp = aNew;
        }
      }
    }
  }

  ~Trine() {
    for (auto &entry : next) {
      delete entry.second;
    }
  }
};

struct RespacerSolution {
  const Trine *trine;
  uint32_t space;
  uint32_t missed;
  list<pair<uint32_t, uint32_t>> words;

  RespacerSolution(const Trine *trine, uint32_t missed) : trine(trine), space(0), missed(missed) {}
};
void printReshaperSolution(const string &text, const RespacerSolution &current, ostream &out) {
  out << "+ " << "{ missed: " << current.missed << ", solution: '";
  for (auto word : current.words) {
    // to check for counting errors
    // auto length = word.first + word.second < text.length() ? word.second : text.length() - word.first;
    out << text.substr(word.first, word.second) << ' ' << flush;
  }
  out << "' }";
}

class TextRespacer {
  const Trine *root;

 public:
  explicit TextRespacer(const Trine *trine) : root(trine) {

  }

  RespacerSolution respace(string text) {
    list<RespacerSolution> currentMatches;
    list<list<RespacerSolution>::iterator> endingSolutionToCompare;
    currentMatches.emplace_back(root, 0);
    for (auto i = 0u; i < text.length(); ++i) {
      char c = text[i];
      auto ending_best_lost_characters = UINT32_MAX;
      auto best_solution_missed_characters = UINT32_MAX;
      for (auto it = currentMatches.begin(); it != currentMatches.end(); it++) {
        auto &solution = *it;

        if (solution.trine->next.count(c) > 0) {
          solution.trine = solution.trine->next.at(c);
          solution.space++;
          if (solution.trine->next.empty()) {
            solution.words.emplace_back((1 + i) - solution.space, solution.space);
            endingSolutionToCompare.emplace_back(it);
            ending_best_lost_characters = min(ending_best_lost_characters, solution.missed);
            solution.space = 0;
            solution.trine = this->root;
          }
        } else {
          if (solution.trine->isWord) {
            // not able to match but we already stopped on a word
            solution.words.emplace_back(i - solution.space, solution.space);
            endingSolutionToCompare.emplace_back(it);
            ending_best_lost_characters = min(ending_best_lost_characters, solution.missed);
            solution.missed++;
          } else {
            solution.missed++;
          }
          solution.trine = this->root;
          solution.space = 0;
        }

        best_solution_missed_characters = min(best_solution_missed_characters, solution.missed);
      }

      auto remaining_characters = (text.length() - i);
      auto would_be_missed_if_we_match_everything_from_now_on = i;
      auto would_new_solution_do_better_than_the_best_losing_now =
          (remaining_characters + best_solution_missed_characters) > would_be_missed_if_we_match_everything_from_now_on;
      if (ending_best_lost_characters < UINT32_MAX) {
        for (auto it_elem = endingSolutionToCompare.rbegin(); it_elem != endingSolutionToCompare.rend(); it_elem++) {
          if ((*it_elem)->missed > ending_best_lost_characters) {
            currentMatches.erase(*it_elem);
          }
        }
        endingSolutionToCompare.clear();
      } else if (root->next.count(c) > 0 && would_new_solution_do_better_than_the_best_losing_now) {
        currentMatches.emplace_back(root, i);
      }
    }

    RespacerSolution best_solution(root, UINT32_MAX);
    for (auto &current : currentMatches) {
      printReshaperSolution(text, current, cout);
      cout << endl;
      if (current.missed < best_solution.missed) {
        best_solution = current;
      }
    }
    return best_solution;
  }
};

int main(int argc, char **argv) {
  Trine trine({"my", "mama", "is", "bigger", "than", "anything"});

  TextRespacer respacer(&trine);
  /**
   * [0,2]
   * [2,4]
   * [6,2]
   * [8,6]
   * [14,4]
   * [18,8]
   */
  auto text = "mymamaisbiggerthananything";
  auto solution = respacer.respace(text);
  // printReshaperSolution(text, solution, cout);
  solution = respacer.respace("yourmamaisbiggerthanmymama");

}
