#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <list>
#include <execution>
using namespace std;

class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
  out << "[ ";
  for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
  out << ']';
  return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
  out << '{' << p.first << ',' << p.second << '}';
  return out;
}
//class MasseuseScheduler;
struct Schedule {
  uint32_t totalTime = 0;
  uint32_t nextAvailability = 0;
  vector<uint32_t> schedule;
};

ostream &operator<<(ostream &out, const Schedule &schedule) {
  out << schedule.totalTime << ' ' << schedule.schedule << ' ' << schedule.nextAvailability << endl;
  return out;
}

class MasseuseScheduler {

 public:

 private:
  list<Schedule> schedules;
  static const uint32_t PAUSE_BREAK;
 public:
  uint32_t createSchedule(std::vector<uint32_t> &jobs) {
    uint32_t currentTime = 0;
    cout << endl;
    for (int lengthNext : jobs) {
      bool found = false;
      cout << "-------" << currentTime << endl;
      for_each(schedules.begin(), schedules.end(), [](auto &item) { cout << item; });
      cout << endl;
      for (auto it = schedules.begin();
           it != schedules.end() && it->nextAvailability <= currentTime;
           it++) {
        found = true;
        it->nextAvailability += 1;
        Schedule copy = *it;
        copy.nextAvailability = currentTime + 2;
        copy.totalTime += lengthNext;
        copy.schedule.push_back(lengthNext);
        Schedule tmp = *it;
        it = schedules.erase(it);

        it = insertInOrder(it, tmp);
        it = insertInOrder(it, copy);
      }
      if (!found) {
        Schedule schedule;
        schedule.nextAvailability = currentTime + 2;
        schedule.totalTime = lengthNext;
        schedule.schedule.push_back(lengthNext);
        insertInOrder(schedules.begin(), schedule);
      }
      currentTime += 1;
    }
    cout << "-------------------------" << endl;
    for_each(schedules.begin(), schedules.end(), [](auto &item) { cout << item; });

    auto solution = std::max_element(schedules.begin(), schedules.end(),
                                     [](auto &it1, auto &it2) {
                                       return it1.totalTime < it2.totalTime;
                                     }
    );
    cout << endl << "found: " << *solution << endl;
    return solution->totalTime;
  }
 private:
  list<Schedule>::iterator insertInOrder(list<Schedule>::iterator it, Schedule &schedule) {
    cout << " @inserting: " << schedule;
    while (it != schedules.end() && it->nextAvailability < schedule.nextAvailability) {
      ++it;
    }
    if (it != schedules.end()) {
      if (it->nextAvailability == schedule.nextAvailability) {
        if (it->totalTime < schedule.totalTime) {
          cout << " @erasing: " << *it << " subs " << schedule << endl;
          auto iterator = schedules.erase(it);
          iterator = schedules.insert(iterator, schedule);
          return iterator;
        }
        return it;
      }
    }
    schedules.push_back(schedule);
    return it;
  }
};

const uint32_t MasseuseScheduler::PAUSE_BREAK = 15;
int main(int argc, char **argv) {
  std::vector<uint32_t> data = {30, 15, 60, 75, 45, 15, 15, 45};
  MasseuseScheduler scheduler;
  cout << "total time: " << scheduler.createSchedule(data) << std::endl;
}