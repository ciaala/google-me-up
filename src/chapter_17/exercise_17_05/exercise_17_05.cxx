#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
    out << '{' << setw(2) << p.first << ',' << setw(2) << p.second << '}';
    return out;
}

class FindSubArrayWithEqualLetterNumber {
public:
    pair<uint32_t, uint32_t> findLongestSequence(const string &letterDigits) {
        pair<uint32_t, uint32_t> solution;
        auto letters = 0u;
        auto digits = 0u;
        for (auto i = 0u; i < letterDigits.size(); ++i) {
            if (isalpha(letterDigits[i])) {
                letters++;
            } else {
                digits++;
            }
        }
        //auto balance = 0;
        vector<pair<uint32_t, int>> tracker;
        auto max_length = 0u;
        for (auto i = 0u; i < letterDigits.size(); ++i) {
            auto isAlpha = isalpha(letterDigits[i]);
            cout << setw(2) << i << ' ' << letterDigits[i] << ':';
            bool _hasAlpha = false;
            bool _hasNumber = false;
            for (pair<uint32_t, int> &t :tracker) {
                if (isAlpha) {
                    t.second++;
                } else {
                    t.second--;
                }
                _hasAlpha |= t.second == 1;
                _hasNumber |= t.second == -1;
                cout << setw(4) << t;
                if (t.second == 0 && max_length < i - t.first) {
                    max_length = i - t.first+1;
                    solution = {t.first, i};
                }
            }
            if (isAlpha && !_hasAlpha) {
                tracker.emplace_back(i, 1);
                cout << setw(4) << tracker.back();
            } else if (!isAlpha && !_hasNumber) {
                tracker.emplace_back(i, -1);
                cout << setw(4) << tracker.back();
            }

            cout << endl;
        }
        cout << max_length << ' ' << solution << endl;
        cout << letters << ' ' << digits << ' ' << letterDigits << endl;
        return {};
    }
};


Dice dice;

string generateStringWithLength(int length) {
    string s;
    s.reserve(length);
    for (auto i = 0u; i < length; ++i) {
        uint8_t r = dice.next(0, 50);
        char c = r < 25u ? 48 + (r / 2.5) : 97 + r - 25;
        s.push_back(c);
    }
    return s;
}


void check(const string &s) {
    FindSubArrayWithEqualLetterNumber finder;
    cout << finder.findLongestSequence(s) << endl;
}


int main(int argc, char **argv) {
    check("1235c456cc");
    check("cc1235c456cc");
    check("p66vl9k04p");
    check(generateStringWithLength(10));
}