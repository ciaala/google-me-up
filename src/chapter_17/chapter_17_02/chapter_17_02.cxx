//
// Created by Francesco Fiduccia on 2019-01-20.
//
#include <vector>
#include <random>
#include <iostream>
#include <array>
#include <string>

using namespace std;

enum Symbol {
    Hearts,
    Spades,
    Clubs,
    Diamonds
};
enum Value {
    Ace,
    _2,
    _3,
    _4,
    _5,
    _6,
    _7,
    _8,
    _9,
    _10,
    Jack,
    Queen,
    King
};
array<Symbol, 4> __symbols{
        Symbol::Clubs,
        Symbol::Diamonds,
        Symbol::Hearts,
        Symbol::Spades
};
array<Value, 13> __values{
        Value::Ace,
        Value::_2,
        Value::_3,
        Value::_4,
        Value::_5,
        Value::_6,
        Value::_7,
        Value::_8,
        Value::_9,
        Value::_10,
        Value::Jack,
        Value::Queen,
        Value::King
};
array<string, 13> __values_names{
        "Ace",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "Jack",
        "Queen",
        "King"
};
array<string, 4> __symbols_names{
        "Clubs",
        "Diamonds",
        "Hearts",
        "Spades"
};

class Card {

public:
    Card(Symbol symbol, Value value) : symbol(symbol), value(value) {}

    Symbol getSymbol() const { return symbol; }

    Value getValue() const { return value; }

private:

    Symbol symbol;
    Value value;

};

uint32_t getNextRandom(uint32_t base, uint32_t maximum) {
    std::default_random_engine generator((unsigned int) clock());
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
}

void shuffleDeck(vector<Card> &deck) {
    for (auto i = 0u; i < deck.size(); ++i) {
        auto k = getNextRandom(0, i);
        auto tmp = deck[k];
        deck[k] = deck[i];
        deck[i] = tmp;
    }
}

ostream &operator<<(ostream &out, Card &card) {
    out << __symbols_names[card.getSymbol()] << '_' <<__values_names[card.getValue()];
    return out;
}
ostream &operator<<(ostream &out, vector<Card> &cards) {
    for (auto &card : cards) {
        out << card << ", ";
    }
    return out;
}


int main(int argc, char **argv) {

    vector<Card> deck;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 13; j++) {
            deck.emplace_back(Card(__symbols[i], __values[j]));
        }
    }
    cout << deck << endl;
    shuffleDeck(deck);
    cout << deck << endl;

}