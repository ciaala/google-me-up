#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

void check(uint32_t number, unsigned int expected);

template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T>
ostream &operator<<(ostream &out, const pair<T, T> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

class CounterOf2s {

private:
    uint32_t count(uint32_t number, uint32_t power) {
        cout << "number: " << number << ", power: " << power << endl;
        if (power == 1) {
            return number > 1 ? 1 : 0;
        }

        auto dec = number / power;
        auto rem = number % power;
        cout << dec << ' ' << rem << endl;

        uint32_t partial;
        if (dec == 1) {
            cout << "A";
            partial = count(power, power / 10);
        } else if (dec > 2) {
            cout << "C";
            partial = dec * count(power, power / 10) + power;
        } else {
            cout << "B";
            partial = dec * count(power, power / 10) + rem + 1;
        }
        auto result = partial + count(rem);
        cout << result;
        return result;
    }

public:
    uint32_t count(uint32_t number) {
        uint32_t power = 1;
        while (power * 10 < number) {
            power *= 10;
        }
        cout << "power: " << power << endl;
        return count(number, power);
    }


};


void check(uint32_t number, unsigned int expected) {
    stringstream ss;
    for (auto v = 1u; v <= number; v++) {
        ss << v;
    }
    string text = ss.str();
    auto checked = 0u;
    auto start = text.find('2', 0);
    while (start != -1) {
        start = text.find('2', start+ 1);
        checked++;
    }

    CounterOf2s counter;
    cout << endl << number << ' ' << checked << endl;
    auto actual = counter.count(number);
    cout << (actual == expected ? "===" : "!!!") << ' ' << number << ' ' << actual << " => " << expected
         << endl;
}

int main(int argc, char **argv) {
    Dice dice;
    check(0, 0u);
    check(1, 0u);
    check(2, 1u);
    check(9, 1u);
    check(10, 1u);
    check(11, 1u);

    check(12, 2u);
    check(19, 2u);
    check(20, 3u);
    check(21, 4u);
    check(22, 6u);
    check(31, 13u);
    check(32, 14u);
    check(52, 16u);
    check(92, 20u);
    check(102, 21u);
    check(112, 22u);
    check(122, 26u);
    //check(dice.next(0, UINT32_MAX), 0u);
}
