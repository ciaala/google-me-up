#include <utility>

/**
 * Given a stair of height X
 * the ability to move 1, 2 or 3 steps at the time
 * Generate all the possible combination to walk up the stair
 */
#include <iostream>
#include <vector>
#include <unordered_map>
#include <iomanip>

using namespace std;
typedef vector<uint32_t> Solution;

class GenerateAllPossibleStairs {
private:
    vector<uint32_t> steps;
    unordered_map<uint32_t, vector<Solution>> memo;
    uint32_t depth;
public:
    GenerateAllPossibleStairs(vector<uint32_t> steps) : steps(std::move(steps)) {

    }

    vector<Solution> generateSolutions(uint32_t height) {
        depth++;
        vector<Solution> results;
        if (height == 0) {
            results.emplace_back();
            return results;
        }
        if (memo.count(height) > 0) {
            cout << setw(2 * depth + 1) << '#' << height << endl;
            return memo[height];
        }
        for (auto &step : steps) {
            if (step <= height) {
                auto solutions = generateSolutions(height - step);
                for (auto &solution : solutions) {
                    auto copy = solution;
                    copy.emplace_back(step);
                    results.emplace_back(copy);
                }
            } else {

            }
        }
        memo[height] = results;
        cout << "~" << height << '@' << results.size() << endl;
        depth--;
        return results;
    }
};

ostream &operator<<(ostream &oStream, vector<uint32_t> &solution) {
    for ( auto it = solution.rbegin(); it != solution.rend(); ++it) {
        oStream << *it << ' ';
    }
    return oStream;
}

int main(int argc, char **argv) {
    GenerateAllPossibleStairs generate({1, 2, 3, 4, 5});
    vector<Solution> solutions = generate.generateSolutions(5);
    if ( solutions.size() < 512) {
        uint32_t indexWide = (uint32_t) log10(solutions.size()) + 1;
        for (auto i = 0u; i < solutions.size(); ++i) {
            auto &solution = solutions[i];
            cout << setw(indexWide) << i << ' ' << solution << endl;
        }
    }
    cout << "Found solutions: " << solutions.size() << endl;
    return 0;
}