#include <iomanip>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <chrono>
#include <functional>
// #include <transform>

using namespace std;
typedef vector<vector<bool>> resultType;


ostream &operator<<(ostream &oStream, const vector<bool> &solution) {
  for (bool isOpen : solution) {
    oStream << (isOpen ? '(' : ')');
  }
  return oStream;
}
ostream &operator<<(ostream &oStream, const resultType &results) {
  for(const auto &result : results) {
    oStream << result << endl;
  }
  return oStream;
}

class FindParentheses {


 private:
  unordered_map<uint64_t, resultType> memo;
 public:
  uint64_t makeKey(uint32_t open, uint32_t close) {
    return (uint64_t) open << 32 | close;
  }


  bool isKeyInMemo(uint32_t open, uint32_t close) {
    return memo.count(makeKey(open, close)) > 0;
  }

  resultType getFromMemo(uint32_t open, uint32_t close) {

    cout << '~' << open << ',' << close << endl;
    return memo[makeKey(open, close)];
  }

  resultType collectAllParentheses(uint32_t n) {

    vector<bool> solution(n * 2, false);
    return collectParentheses(solution, 0, 0, n);
  }

  void setInMemo(uint32_t open, uint32_t close, resultType result) {
    memo[makeKey(open, close)] = std::move(result);
  }

  resultType collectParentheses(vector<bool> &solution,
                                uint32_t open,
                                uint32_t close,
                                uint32_t count) {
    resultType results;
    if (open == count && close == count-1) {
      //results.push_back(solution);
      return {{false}};
    }

   // if (isKeyInMemo(open, close)) {
   //   return getFromMemo(open, close);
   // }
    if (open < count) {

      vector<bool> copy = solution;
      copy[open + close] = true;
      // cout << setw(open + close + 1) << 'o' << copy << endl;

      auto solutions = collectParentheses(copy, open + 1, close, count);

      transform(results.end(), solutions.begin(), solutions.end(),);

    }

    if (close < open) {
      // vector<bool> copy = solution;
      // cout << setw(open + close + 1) << 'c' << copy << endl;

      auto solutions = collectParentheses(copy, open, close + 1, count);
      copy[open + close] = false;

      results.insert(results.end(), solutions.begin(), solutions.end());
    }

    setInMemo(open, close, results);
    return results;
  }
};

int main(int argc, char **argv) {
  FindParentheses solver;



  auto start = chrono::system_clock::now();
  auto results = solver.collectAllParentheses(20        );
  auto finish = chrono::system_clock::now();
  auto time = chrono::duration_cast<chrono::milliseconds>(finish - start).count();

  cout << results << endl;
  cout << "Solutions: " << results.size() << endl;
  cout << "Execution time: " << time << "ms" << endl;

  return 0;
}
