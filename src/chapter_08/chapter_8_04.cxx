#include <vector>
#include <iostream>
#include <stack>
#include <cmath>
#include <iomanip>

//
// Write a function to return all the subset of a function
//
using namespace std;
typedef vector<uint64_t> Solution;

vector<Solution> generateSubset(
        vector<uint64_t> set,
        uint64_t index = 0) {
    vector<Solution> results;
    if (index == (set.size() - 1)) {
        return {{},
                {set[index]}};
    }
    vector<Solution> subsets = generateSubset(set, index + 1);
    results.reserve(2 * subsets.size());
    for (auto &one : subsets) {
        Solution zero = one;
        results.push_back(zero);
        one.push_back(set[index]);
        results.push_back(one);
    }
    return results;
}

ostream &operator<<(ostream &oStream, Solution &solution) {
    for (auto &element: solution) {
        oStream << element << ' ';
    }
    oStream << endl;
    return oStream;
}

int main(int argc, char **argv) {
    vector<uint64_t> set = {0, 1, 2, 3, 4, 5, 6};
    auto results = generateSubset(set);
    if (results.size() < 256) {
        const uint32_t sizeDigits = (uint32_t) log10(results.size()) + 1;
        for (uint32_t i = 0u; i < results.size(); ++i) {
            cout << setw(sizeDigits) << i << "] " << results[i] << endl;
        }
    }
    cout << "Solutions: " << results.size() << endl;

    return 0;
}
