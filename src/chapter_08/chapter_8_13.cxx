/**
 * Stack of Boxes
 */
#include <vector>
#include <unordered_map>
#include <deque>
#include <ostream>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <random>
#include <functional>
#include <iostream>
#include <chrono>
#include <assert.h>

using namespace std;
#define SAMPLES 800
#undef SHOULD_VERIFY_SOLUTION
#undef LOGGING
class Box {

 public:
  uint32_t width;
  uint32_t depth;
  uint32_t height;
  Box(uint32_t width,
      uint32_t depth,
      uint32_t height) : width(width), height(height), depth(depth) {
  }
  friend ostream &operator<<(ostream &oStream, const Box &box) {
    oStream << "{" << setfill(' ') << setw(2) << box.width << "," << setw(2) << box.depth << "," << setw(2)
            << box.height << "}";
    return oStream;
  }
};
ostream &operator<<(ostream &oStream, const vector<Box *> &boxes) {
  oStream << "[";
  if (boxes.size() == 0) {
    oStream << "[]";
    return oStream;
  }
  oStream << "[";
  for (int i = 0; i < boxes.size(); i++) {
    auto box = boxes[i];
    if (box != nullptr) {
      oStream << *box << ", ";
    } else {
      oStream << "null" << ", ";
    }
  }
  if (boxes.size() > 0) {
    oStream << *boxes.back();
  }
  oStream << "]";
  return oStream;
}

static const function<bool(Box *, Box *)> orderByWidth = [](Box *o1, Box *o2) {
  return o1->width > o2->width;
};
static const function<bool(Box *, Box *)> orderByDepth = [](Box *o1, Box *o2) {
  return o1->depth > o2->depth;
};

static const function<bool(Box *, Box *)> orderByArea = [](Box *o1, Box *o2) {
  return ((o1->depth * o1->width) > (o2->depth * o2->width));
};
static const function<Box *(Box &)> returnPointer = [](Box &item) { return &item; };

class Solution {

 private:
  unordered_map<Box *, uint32_t> memo;
  vector<Box *> current;
  unordered_map<Box *, vector<Box *>> best;

  // Configuration
  bool useShortSearchDepth;
  bool searchByArea;

#ifdef LOGGING
  const bool LOG_LEVEL;
  uint32_t wide;

#endif

  vector<Box *> areaSorted;
#ifdef SHOULD_VERIFY_SOLUTION
  vector<Box *> widthSorted;
  vector<Box *> depthSorted;
#endif


  uint32_t
  assemble(vector<Box *> &boxes, uint32_t index) {
    static uint32_t depth = 0;
    auto base = boxes.at(index);
#ifdef LOGGING
    if (LOG_LEVEL) {
      current.push_back(base);
      depth++;
    }
#endif
    if (memo.count(base) > 0) {

#ifdef LOGGING

      if (LOG_LEVEL) {
        depth--;
        cout << setw(depth) << setfill('-') << '-' << "@" << index << "@" << memo[base] << endl;
        current.pop_back();
      }
#endif
      return memo[base];
    }

#ifdef LOGGING
    if (LOG_LEVEL) std::cout << setw(depth) << setfill('-') << *base << "[" << index << "]" << endl;
#endif
    uint32_t maxValue = base->height;
    for (uint32_t ii = index + 1; ii < boxes.size(); ++ii) {
      auto box = boxes.at(ii);

      if ((box->width < base->width) && (box->depth < base->depth)) {
        uint32_t value = base->height + assemble(boxes, ii);
        maxValue = maxValue > value ? maxValue : value;
      }

    }

    memo[base] = maxValue;
#ifdef LOGGING
    if (LOG_LEVEL) {
      depth--;
      best[base] = current;
      current.pop_back();
    }
#endif
    return maxValue;
  }


  uint32_t stackBoxesByArea() {
    uint32_t maxHeight = 0;

    for (uint32_t i = 0; i < areaSorted.size(); i++) {
      auto height = assemble(areaSorted, i);
      maxHeight = maxHeight > height ? maxHeight : height;
#ifdef LOGGING
      if (LOG_LEVEL) {
        const auto box = areaSorted[i];
        cout << setw(wide) << i << " " << *box << best[box] << "->" << height << endl;
        best[box] = current;
        current.clear();
      }
#endif
    }
    return maxHeight;
  }
 public:
  Solution(bool useShortSearchDepth,
      bool searchByArea,
      bool logging = false
          )
      : useShortSearchDepth(useShortSearchDepth)
        , searchByArea(searchByArea)
#ifdef LOGGING
        , LOG_LEVEL(logging)
#endif
  {
  }

#ifdef SHOULD_VERIFY_SOLUTION
  uint32_t
  stackBoxesDimensions() {
    uint32_t maxHeight = 0;

    for (uint32_t i = 0; i < widthSorted.size(); i++) {
      auto height = assemble(widthSorted, i);
      const auto box = widthSorted[i];

      best[box] = current;
      current.clear();
#ifdef LOGGING
      if (LOG_LEVEL) cout << setw(wide) << i << " " << *box << best[box] << "->" << height << endl;
#endif

      maxHeight = maxHeight > height ? maxHeight : height;

    }
#ifdef LOGGING
    if (LOG_LEVEL) print_caches();
    if (LOG_LEVEL) cout << "Width: " << maxHeight << endl;
#endif
    auto largest = widthSorted.at(0);
    best.clear();
    memo.clear();
    current.clear();
    for (uint32_t i = 0; i < depthSorted.size(); i++) {

      if (this->useShortSearchDepth && depthSorted.at(i) == largest) {
        return maxHeight;
      }

      auto height = assemble(depthSorted, i);
      maxHeight = height > maxHeight ? height : maxHeight;

    }
#ifdef LOGGING
    if (LOG_LEVEL) print_caches();
#endif
    return maxHeight;
  }
#endif

void prepare(vector<Box> &boxes) {
    const auto boxCount = boxes.size();
#ifdef LOGGING
    wide = 1u + (uint32_t) log10(boxCount);
#endif
    uint32_t value = 16u + (uint32_t) ((double) boxCount * 1.25);
    memo.clear();
    memo.reserve(value);
    if (this->searchByArea) {
      areaSorted.resize(boxCount);
      transform(boxes.begin(), boxes.end(), areaSorted.begin(), returnPointer);
      sortVector(areaSorted, orderByArea);
    }
#ifdef SHOULD_VERIFY_SOLUTION
    else
      widthSorted.resize(boxCount);
      depthSorted.resize(boxCount);
      transform(boxes.begin(), boxes.end(), widthSorted.begin(), returnPointer);
      transform(boxes.begin(), boxes.end(), depthSorted.begin(), returnPointer);

      sortVector(widthSorted, orderByWidth);
      sortVector(depthSorted, orderByDepth);
    }
#endif
}

  uint32_t stackBoxes() {
#ifdef SHOULD_VERIFY_SOLUTION
    if (!this->searchByArea) {
      return stackBoxesDimensions();
    } else {
      return stackBoxesByArea();
    }
#else
    return stackBoxesByArea();
#endif
  }

  void sortVector(vector<Box *> &sorted,
                  const function<bool(Box *, Box *)> &orderFunction) {
    sort(sorted.begin(), sorted.end(), orderFunction);
#ifdef LOGGING
    if (LOG_LEVEL) {
      cout << sorted << endl;
      cout << setw(12) << setfill('-') << "" << endl;
    }
#endif
  }
#ifdef LOGGING

  void print_caches() {
    if (LOG_LEVEL) {
      for (auto it = best.cbegin(); it != best.cend(); ++it) {
        cout << *(it->first) << " -> " << it->second << " = " << memo[it->first] << endl;
      }
    }
  }
#endif

};

void printExecutionTimes(const array<long, 800> &executionTimes);
int main(int argc, char **argv) {

  vector<Box> boxes;
  boxes.reserve(SAMPLES);

  std::default_random_engine generator;
  std::uniform_int_distribution<int> distribution(1, 80);
  auto dice = std::bind(distribution, generator);
  // std::cout.setstate(std::ios_base::badbit);

#ifdef SHOULD_VERIFY_SOLUTION
  uint32_t previous = 0;
#endif
  array<long, SAMPLES> executionTimes;
  Solution solutionArea(false, true);

  for (int i = 0; i < SAMPLES; ++i) {

    boxes.emplace_back(dice(), dice(), dice());

    solutionArea.prepare(boxes);
    auto start = std::chrono::high_resolution_clock::now();

    const auto maxHeightArea = solutionArea.stackBoxes();

#ifdef SHOULD_VERIFY_SOLUTION
    Solution solutionWidth(false, false, i);
    Solution solutionDepth(true, false, i);
    solutionWidth.prepare(boxes);
    solutionDepth.prepare(boxes);
    const auto maxHeightWidth = solutionWidth.stackBoxes();
    const auto maxHeightDepth = solutionDepth.stackBoxes();

    if (maxHeightDepth != maxHeightArea
        || maxHeightWidth != maxHeightArea
        || maxHeightDepth != maxHeightWidth
        || previous > maxHeightArea) {
      cerr << "Error for :" << i + 1 << maxHeightArea << ", " << maxHeightDepth << ", " << maxHeightWidth << endl;
    }
#endif
    auto finish = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count();
    executionTimes[i] = time;

#ifdef SHOULD_VERIFY_SOLUTION
    previous = maxHeightArea;
#endif
  }
  printExecutionTimes(executionTimes);
  // cerr << (uint64_t)((double)total / 800.0);
  // cout << endl << "Solution: " << maxHeight << endl;
  return 0;

}
void printExecutionTimes(const array<long, SAMPLES> &executionTimes) {
  uint64_t total = 0;
  for (int i = 0; i < executionTimes.size(); i++) {
    auto time = executionTimes[i];
    // cout.imbue(locale(""));
    cout << i + 1 << '\t' << time << endl;
    total += time;
  }
}

