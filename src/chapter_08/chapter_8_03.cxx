#include <iostream>
#include <iomanip>
#include <random>
#include <algorithm>
#include <functional>
using namespace std;
// Look for Magic Index

class MagicIndexSolver {

public:
    uint32_t solve(vector<uint32_t> vs,
                   uint32_t min,
                   uint32_t max) {
        auto index = (min + max) / 2;
        auto value = vs[index];

        if (index == min && index == max) {
            return UINT32_MAX;
        }
        if (index == value) {
            return index;
        } else if (value < min) {
            return solve(vs, index, max);
        } else if (value > max) {
            return solve(vs, min, index);
        }
        if (min != index) {
            auto m = solve(vs, min, index - 1);
            if (m != UINT32_MAX) {
                return m;
            }
        }
        if (max != index) {
            auto n = solve(vs, index + 1, max);
            if (n != UINT32_MAX) {
                return n;
            }
        }
        return UINT32_MAX;
    }
};

ostream &operator<<(ostream &oStream, vector<uint32_t> &solution) {
    for (auto &element: solution) {
        oStream << element << ' ';
    }
    oStream << endl;
    return oStream;
}

int main(int argc, char **argv) {
    const uint32_t TEST_DATA_VECTOR = 25;
    std::default_random_engine generator;
    std::uniform_int_distribution<uint32_t> distribution(0, 4 * TEST_DATA_VECTOR);
    auto dice = std::bind(distribution, generator);
    for (uint32_t n = 0; n < 256; ++n) {
        MagicIndexSolver solver;
        const uint32_t testDataVector = TEST_DATA_VECTOR + n;

        vector<uint32_t> data(testDataVector);
        for (auto i = 0u; i < testDataVector; ++i) {
            data[i] = dice();
        }
        sort(data.begin(), data.end());
        uint32_t result = solver.solve(data, 0, testDataVector);
        if (result != UINT32_MAX) {
            cout << data << endl;
            cout << "Found: " << data[result] << '@' << result << " with: " << n << endl;
        }
    }
    return 0;
}