/*
NOTES
    Google C++ Rules:
        - Naming Convention
        - Functional Interface
        - Functional Nomenclature
    STRING
      - Comparison of String
      - Format string in C++ beyond stringstream and printf
      - primitive types conversion
    array
      - is a glorified arrays with length

8.14
*/
#include <string>
#include <unordered_map>
#include <array>
#include <iostream>
#include <vector>

using namespace std;


class Solution {
private:
/*
    class ResultStep {
    public:

    };
    typedef ResultStep RS;
*/
public:
    vector<uint32_t> getOperations(string &expression) {
        vector<uint32_t> results;
        for (uint32_t i=0; i < expression.size(); ++i) {
            char c  = expression.at(i);
            if (c != '1' && c != '0') {
                results.emplace_back(i);
            }
        }
        return results;
    };

    uint32_t countParenthesis(string &expression, bool expectedResult) {
        vector<uint32_t> ops = getOperations(expression);

        unordered_map<string, array<uint32_t, 2>> memo;

        array<uint32_t, 2> results = buildEval(expression, ops, 0, ops.size() - 1, memo );

        return results[expectedResult];
    }

private:

    array<uint32_t, 2>
    buildEval(string &expression,
            vector<uint32_t> &ops,
            uint32_t start,
            uint32_t end,
            unordered_map<string, array<uint32_t, 2>> & memo)
    {
        cout << "buildEval " << start << " - " << end << " memo " << memo.size() << endl;
        array<uint32_t, 2> results =  {0,0};
        for(uint32_t i=start; i <= end; i++ ) {
            array<uint32_t, 2> lp1 = {0,0};
            array<uint32_t, 2> lp2 = {0,0};

              if ( i == start) {
                  char c = expression[ ops[i] - 1 ];
                  if (c == '1') {
                      lp1[1] = 1;
                    } else {
                      lp1[0] = 1;
                    }
                    cout << "@" << ops[i]-1 << " -> " << expression.substr(0, ops[i]-1) << "_" << c << "_" << expression.substr(ops[i]) << endl;
              } else {
                  string key = to_string(start) + "-" + to_string(i-1) + "-" + to_string(end);
                if ( memo.count(key) > 0 ){
                  lp1 =  memo[key];
                } else {
                  lp1 = buildEval(expression, ops, start, i-1, memo);
                }
              }

              if ( i == end ) {
                char c = expression[ ops[i] + 1 ];
                if (c == '1') {
                  lp2[1] = 1;
                } else {
                  lp2[0] = 1;
                }
                cout << "@" << ops[i]+1 << " -> " << expression.substr(0, ops[i]+1) << "_" << c << "_" << expression.substr(ops[i]+2) << endl;
              } else {
                // TODO format string in C++
                string key = to_string(start) + "-" + to_string(i-1) + "-" + to_string(end);
              if ( memo.count(key) > 0 ){
                lp2 =  memo[key];
              } else {
                lp2 = buildEval(expression, ops, i+1, end, memo);
              }
          }
          char op = expression[ops[i]];
          for (int j = 0; j <= 1; j++ ) {
            if( lp1[j] > 0) {
              for(int k = 0; k <= 1; k++) {
                if ( lp2[k] > 0 ) {

                  bool result = op == '&' ? k && j:
                           op == '|' ? k || j :
                            k ^ j ;
                  auto count = (lp1[j] * lp2[k]);
                  cout << "-> " <<op << "(" << j << ","  << k << ")"
                       << "{"<<lp1[j] << "," << lp2[k] << "} = "
                       << result << "{" << count << "}" << endl;
                  results[result] += count;
                }
              }
            }
          }
        }
          // auto key = tostring(start) + "-" + i + "";
          //memo.put[key];
          return results;
      }

};

int main(int argc, char** argv ) {
    cout << "argv" << argv[0] << endl;
    string expression;
    bool expectedResult;
    if (argc == 1 ) {
        string input;
        cout << "- input> " << flush;
        cin >> input;
        expression = input;
        cout << "- output " << flush;
        cin >> input;
        expectedResult = !input.compare("1");
        // STRING COMPARE IN C++ input

    } else {
        expression = argv[1];
        if ( argc == 3) {
            expectedResult = argv[2][0] == '1';
        } else {
            expectedResult = 0;
        }
    }
    Solution solution;
    cout << "expression: "<< expression << ", expectedResult: " << expectedResult << endl << flush;
    auto result = solution.countParenthesis(expression, expectedResult);
    cout << "Found: "<< result << endl;
    return 0;
}
