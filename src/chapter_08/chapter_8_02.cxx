// ROBOT in a Grid
#include <iostream>
#include <vector>
#include <random>
#include <iomanip>
#include <array>

enum Move {
    RIGHT,
    DOWN
};
using namespace std;


void printMap(vector<bool> &mapFlags,
              const uint32_t rows,
              const uint32_t columns,
              vector<Move> &path) {
    vector<uint32_t> step = {0u, 0u};
    auto pathStep = path.begin();
    cout << setw(columns + 2) << setfill('-') << '-' << endl;
    for (auto j = 0u; j < rows; ++j) {
        cout << '|';
        auto baseRow = columns * j;
        for (auto i = 0u; i < columns; ++i) {
            if (step[0] == i && step[1] == j) {
                if (pathStep != path.end()) {
                    Move move = *pathStep;

                    pathStep++;
                    char m = (move == RIGHT) ? 'R' : 'D';
                    if (move == DOWN) {
                        step[1]++;
                    } else {
                        step[0]++;
                    }
                    cout << m;
                } else {
                    cout << 'X';
                }
            } else {
                auto c = (mapFlags[baseRow + i] ? ' '
                                                : '*');
                cout << c;
            }

        }
        cout << '|' << endl;
    }
    cout << setw(columns + 2) << setfill('-') << '-' << endl;

}

class MakeRobotMove {
public:
    vector<Move> discoverPath(
            vector<bool> mapFlags,
            uint32_t rows,
            uint32_t columns) {
        array<bool,2> initial = {true, true};
        vector<array<bool,2>> moves(rows*columns, initial);
        vector<Move> result;
        auto rm = rows;
        auto cm = columns;
        auto x = 0u, y = 0u;
        while (x != (rows - 1) || y != (columns-1)) {
            if (cm > 0 && moves[y*columns+ x][0]) {
                if (!mapFlags[columns * y + x + 1]) {
                    moves[y*columns+ x][0] = false;
                } else {
                    ++x;
                    --cm;
                    result.emplace_back(RIGHT);
                }
            } else if (cm >= 0 && moves[y*columns+ x][1]) {
                if (!mapFlags[columns * (y + 1) + x]) {
                    moves[y*columns+ x][1] = false;
                } else {
                    --rm;
                    ++y;
                    result.emplace_back(DOWN);
                }
            } else if (result.empty()) {
                // No solution found
                return {};
            } else {
                Move &move = result.back();
                result.pop_back();
                if (move == RIGHT) {
                    --x;
                    ++cm;
                    moves[y*columns+ x][0] = false;
                } else {
                    --y;
                    ++rm;
                    moves[y*columns+ x][1] = false;
                }
            }
            printMap(mapFlags, rows, columns, result);
            cout << setw(3 * columns) << setfill('=') << '=' << endl;
        }
        return result;
    }
};


void makeMap(vector<bool> &mapFlags, uint32_t rows, uint32_t columns) {
    default_random_engine generator_x(generator_x.default_seed);
    default_random_engine generator_y(generator_x.default_seed + 1);
    uniform_int_distribution<uint32_t> distribution_x(0, columns);
    uniform_int_distribution<uint32_t> distribution_y(0, rows);
    auto dice_x = bind(distribution_x, generator_x);
    auto dice_y = bind(distribution_y, generator_y);

    for (auto i = 0u; i < columns + rows; ++i) {
        auto x = dice_x();
        auto y = dice_y();
        mapFlags[columns * y + x] = false;
    }
}

int main(int argc, char **argv) {
    MakeRobotMove pathFinder;
    const uint32_t rows = 16u, columns = 16u;
    vector<bool> mapFlags(rows * columns, true);

    makeMap(mapFlags, rows, columns);

    auto path = pathFinder.discoverPath(mapFlags, rows, columns);
    //vector<Move> path;
    printMap(mapFlags, rows, columns, path);
}
