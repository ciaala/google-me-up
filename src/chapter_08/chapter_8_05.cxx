
#include <iostream>
#include <iomanip>
#include <vector>
#include <array>
/**
Recursive Multiply
 */

using namespace std;

class RecursiveMultiply {

    uint64_t callDepth = 0u;

public:
    RecursiveMultiply() {

    }

    uint64_t multiply(uint64_t a, uint64_t b) {
        cout << setw(2*callDepth+1) << ' ' << "(* " << a << ' ' << b << " )" << endl;
        callDepth++;

        if (b == 0) {
            return 0;
        }
        if (b == 1) {
            return a;
        }
        auto shifter = 0u;
        uint64_t multiplier;
        do {
            shifter++;
            multiplier = 1u << shifter;
        } while ( b >= multiplier);
        shifter -= 1;
        return (a << shifter) + multiply(a, b - (1u << shifter));
    }
};

typedef array<uint64_t, 2> TestData;

int main(int argc, char **argv) {

    TestData datas[] = {
            {0,  0},
            {0,  1},
            {1,  0},
            {1,  1},
            {1,  2},
            {12, 2},
            {2, 12},
            {2,  1},
            {5,  13},
            {13, 5}
    };

    for (auto &testData :datas) {

        auto op1 = testData[0];
        auto op2 = testData[1];
        RecursiveMultiply recMultiply;
        auto result = recMultiply.multiply(op1, op2);
        if (result != (op1 * op2)) {
            cout << "Wrong: " << op1 << " * " << op2 << "=" << result << endl;
        }
    }
    return 0;

}
