#include <vector>
#include <iostream>

using namespace std;

/*
Tower of Hanoi
*/

class HanoiSolution {
private:
    vector<uint32_t> initial;
    vector<uint32_t> support;
    vector<uint32_t> destination;

    void move(vector<uint32_t> &i, vector<uint32_t> &d) {
        auto t = i.back();
        i.pop_back();
        d.push_back(t);
    }

    void shift(uint64_t depth,
               vector<uint32_t> &i,
               vector<uint32_t> &s,
               vector<uint32_t> &d) {
        if (depth == 0) {
            return;
        }
        shift(depth - 1, i, d, s);
        move(i, d);
        shift(depth - 1, s, i, d);
    }

public:
    HanoiSolution(const uint32_t initialHeight) :
            initial(initialHeight),
            support(initialHeight),
            destination(initialHeight) {
        for (auto i = 0u; i < initialHeight; ++i) {
            initial[i] = i;
        }
    }

    void solve() {
        shift(initial.size(), initial, support, destination );
    }

};

ostream &operator<<(ostream &oStream, vector<uint32_t> &data) {

    if (data.size() == 0) {
        oStream << "[]";
    }
    oStream << "[ ";
    for (auto i = 0u; i < data.size() - 1; ++i) {
        auto &item = data[i];
        oStream << item << ", ";
    }
    oStream << data.back() << " ]";
    return oStream;
}

int main(int argc, char **argv) {
    HanoiSolution hanoiSolution(7);
    hanoiSolution.solve();
    return 0;
}