#include <iostream>
#include <array>
#include <stack>
#include <vector>
#include <iomanip>
#include <random>
#include <functional>
#include <chrono>

using namespace std;

typedef array<uint32_t, 2> V2D;

class Solution {

  struct PixelIndex {
    uint32_t seq;
    uint32_t x;
    uint32_t y;
    uint32_t width;
   public:
    PixelIndex(uint32_t seq,
               uint32_t x,
               uint32_t y,
               uint32_t width)
        : seq(seq), x(x), y(y), width(width) {}

    inline PixelIndex right() {
      return PixelIndex(seq + 1, x + 1, y, width);
    }
    inline PixelIndex left() {
      return PixelIndex(seq - 1, x - 1, y, width);
    }
    inline PixelIndex top() {
      return PixelIndex(seq + width, x, y + 1, width);
    }
    inline PixelIndex bottom() {
      return PixelIndex(seq - width, x, y - 1, width);
    }
  };
 public:

  void fillWithColor(vector<uint32_t> &img,
                     V2D imgSize,
                     uint32_t newColor,
                     uint32_t x, uint32_t y) {

    auto width = imgSize[0];
    const auto height = imgSize[1];
    uint32_t pixelCount = width * height;

    const uint32_t pixelIndex = y * width + x;

    vector<uint8_t> pixelFlags(pixelCount, 0);
    pixelFlags[pixelIndex] = 1;
    auto originalColor = img[pixelIndex];

    stack<PixelIndex> pixelIndexQueue;
    pixelIndexQueue.push(PixelIndex(pixelIndex, x, y, width));

    while (!pixelIndexQueue.empty()) {

      PixelIndex index = pixelIndexQueue.top();
      pixelIndexQueue.pop();
      if (img[index.seq] == originalColor) {
        img[index.seq] = newColor;
        tryToEnque(index, pixelFlags, imgSize, pixelIndexQueue);
      }
    }
  }

  void tryToEnque(PixelIndex &pixelIndex,
                  vector<uint8_t> &pixelFlags,
                  V2D &imageSize,
                  stack<PixelIndex> &pixelIndexQueue) {
    //cout << '[' << pixelIndex.x << ',' << pixelIndex.y << ']' << '@' << pixelIndex.seq << '#' << pixelIndexQueue.size() << endl;


    PixelIndex right = pixelIndex.right();
    if (right.x < imageSize[0] && pixelFlags[right.seq ] == 0) {
      pixelFlags[right.seq] = 1;
      pixelIndexQueue.push(right);
    }

    PixelIndex left = pixelIndex.left();
    if (left.x >= 0 && pixelFlags[left.seq] == 0) {
      pixelFlags[left.seq] = 1;
      pixelIndexQueue.push(left);
    }

    PixelIndex top = pixelIndex.top();
    if (top.y < imageSize[1] && pixelFlags[top.seq] == 0) {
      pixelFlags[top.seq] = 1;
      pixelIndexQueue.push(top);
    }

    PixelIndex bottom = pixelIndex.bottom();
    if (bottom.y >= 0 && pixelFlags[bottom.seq] == 0) {
      pixelFlags[bottom.seq] = 1;
      pixelIndexQueue.push(bottom);
    }
  }
};
void printImage(vector<uint32_t> &image, V2D &size) {
  // https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
  // BLU Palette array<uint32_t,5> colorIndexes8Bit = {17,18,19,20,21};
  array<uint32_t, 5> colorIndexes8Bit = {31, 92, 95, 35, 43};

  string escapeSequence = "\033[38;5;";
  string resetSequence = "\033[39;49m";
  cout << setw(size[0] + 2) << setfill('-') << '-' << endl;
  for (int j = 0; j < size[1]; ++j) {
    auto rowStart = size[0] * j;
    cout << '|';
    for (int i = 0; i < size[0]; ++i) {
      uint32_t colorIndex = image[rowStart + i];
      colorIndex = colorIndex < colorIndexes8Bit.size() ? colorIndex : colorIndexes8Bit.size() - 1;
      cout << escapeSequence << colorIndexes8Bit[colorIndex] << "m*";
    }
    cout << resetSequence << '|' << endl;
  }
  cout << setw(size[0] + 2) << setfill('-') << '-' << endl;

}

void runFillWithColor(vector<uint32_t> &image,
                      V2D &imgSize,
                      uint32_t newColor,
                      uint32_t x,
                      uint32_t y,
                      Solution &solution) {
  auto start = chrono::system_clock::now();
  solution.fillWithColor(image, imgSize, newColor, x, y);
  auto finish = chrono::system_clock::now();
  auto time = chrono::duration_cast<chrono::milliseconds>(finish - start).count();
  cout << "Execution time: " << time << "ms" << endl;
}
int main(int argc, char **argv) {

  std::default_random_engine generator;
  std::uniform_int_distribution<uint32_t> distribution(0, 1);
  auto dice = std::bind(distribution, generator);

  V2D imgSize = {32, 16};
  Solution solution;
  vector<uint32_t> image(imgSize[0] * imgSize[1], 0);
  for (auto i = 0u; i < image.size(); ++i) {
    image[i] = dice();
  }
  printImage(image, imgSize);
  runFillWithColor(image, imgSize, 5, 8, 4, solution);
  printImage(image, imgSize);
  runFillWithColor(image, imgSize, 5, 8, 4, solution);
  printImage(image, imgSize);
  return 0;
}
