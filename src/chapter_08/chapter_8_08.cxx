#include <vector>
#include <iostream>
#include <unordered_map>
#include <iomanip>
#include <unordered_set>

using namespace std;

/**
Generate all the iteration of strings

*/

class Solution {
private:
    unordered_map<string, vector<string>> memo;
public:
    vector<string> generateReshuffles(string s) {
        vector<string> results;
        if (s.length() == 1) {
            return {s};
        }
        if (memo.count(s) > 0) return memo[s];

        for (auto i = 0u; i < s.length(); ++i) {
            auto c = s[i];
            auto sub = s.substr(0, i) + s.substr(i + 1);
            auto solutions = generateReshuffles(sub);
            for (auto s: solutions) {
                results.emplace_back(c + s);
            }
        }
        memo[s] = results;
        return results;
    }
    vector<string> generateReshufflesUnique(string s) {
        vector<string> results;
        if (s.length() == 1) {
            return {s};
        }
        if (memo.count(s) > 0) return memo[s];

        unordered_set<char> usedCharacter;
        for (auto i = 0u; i < s.length(); ++i) {

            auto c = s[i];
            if ( usedCharacter.count(c) == 0 ) {
                auto sub = s.substr(0, i) + s.substr(i + 1);
                auto solutions = generateReshufflesUnique(sub);
                for (auto &solution: solutions) {
                    results.emplace_back(c + solution);
                }
                usedCharacter.emplace(c);
            }

        }
        memo[s] = results;
        return results;
    }
};

int main(int argc, char **argv) {
    Solution solution;
    vector<string> results = solution.generateReshufflesUnique("abcdeff");
    if (results.size() < 256) {
        for (auto i = 0u; i < results.size(); i++) {
            cout << setw(3) << 1+i << ' ' << results[i] << endl;
        }
    } else {
        cout << "Solutions: " << results.size() << endl;
    }
    return 0;
}
