class Solution {

public:
    void collectAllParenthesis(uint32_t n) {
        vector<array<bit, n>> results;
        collectParantheses( 0,0,n, results);
        return results;
    }

    collectParenthesis(uint32_t open, uint32_t close, uint32_t count) {
        vector<array<bit>>  results;
        if (open == count && close == count) {

            return;
        }

        if (memo.count(pair<open,close>)) {
        return memo.get(pair<open,close>);
        }

        if (close <open) {

            auto collect = collectParenthesis(open, close + 1 );
            foreach(auto item: collect) {
                auto copy= item;
                copy.set(open+close) = 0;
                results.emplace_back(item);

            }
        }

        if (open < n) {

            auto collec = collectParenthesis( open + 1, close );
            foreach( auto item: collect ) {
                auto copy = item;
                copy.set(open+close) = 1;
                results.emplace_back(copy);
            }
        }
        memo.put(pair<open,close>, results);
    }
};
