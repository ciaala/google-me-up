#include <array>
#include <iostream>
#include <vector>
#include <iomanip>
#include <cassert>
using namespace std;
#define BOARD_SIZE 16
#undef LOGGING
typedef array<uint8_t, 2> position;
ostream &operator<<(ostream &oStream, const position &p) {
  oStream << '(' << (int) p[0] << ", " << (int) p[1] << ')';
  return oStream;
}
ostream &operator<<(ostream &oStream, const vector<position> &positions) {
  oStream << '[';
  if (!positions.empty()) {
    oStream << ' ';
    uint32_t size = (uint32_t) positions.size() - 1;
    for (int i = 0; i < size; ++i) {
      position p = positions[i];
      oStream << p << ", ";
    }
    oStream << positions.back() << ' ';
  }
  oStream << ']';
  return oStream;
}
template<uint32_t SIZE>
class PlaceQueens {
 private:

  vector<vector<position>> placeQueen(
      //    uint32_t toPlace,
      vector<position> &positions,
      //   array<uint8_t, SIZE> &columns,
      array<uint8_t, SIZE> &rows,
      array<uint8_t, 2 * SIZE - 1> &diag1,
      array<uint8_t, 2 * SIZE - 1> &diag2,
      uint8_t i = 0) {

    vector<vector<position>> results;
    auto placed = false;
    for (uint8_t j = 0; j < SIZE; ++j) {
      const auto indexDiag1 = getDiagLR(i, j);
      const auto indexDiag2 = getDiagRL(i, j);

      if ((rows[j] != 1
          && (diag1[indexDiag1] != 1)
          && (diag2[indexDiag2] != 1))
          ) {
        position p = {i, j};
        positions.emplace_back(p);
        if (i < (SIZE - 1)) {
          // columns[i] = 1;
          rows[j] = 1;
          diag1[indexDiag1] = 1;
          diag2[indexDiag2] = 1;
          //  auto copy = positions;
          auto result = placeQueen(positions, rows, diag1, diag2, i + 1);
          results.insert(results.end(), result.begin(), result.end());

          positions.pop_back();
          diag1[indexDiag1] = 0;
          diag2[indexDiag2] = 0;
          rows[j] = 0;
          //columns[i] = 0;
          placed = true;
        } else {
#ifdef LOGGING

          // toPlace = 0;
           // cout << toPlace << setw(2*(1 + SIZE - toPlace)) << setfill(' ') << "" << "Placing " << p << " -> "
           //results.push_back(p);
           printSolution(positions);
#endif
          results.push_back(positions);
          positions.pop_back();
          return results;
        }
      }
    }
    return results;
  }
 public:

  uint32_t getDiagLR(uint32_t i, uint32_t j) {
    assert(i < SIZE);
    assert(j < SIZE);
    return i + j;
  }
  uint32_t getDiagRL(uint32_t i, uint32_t j) {
    assert(i < SIZE);
    assert(j < SIZE);
    return SIZE + j - i - 1;
  }

  /**
   * solve the problems
   * @return
   */
  vector<vector<position>> solve() {
    vector<position> positions = {};
    //  array<uint8_t, SIZE> columns = {};
    array<uint8_t, SIZE> rows = {};
    array<uint8_t, 2 * SIZE - 1> diag1 = {};
    array<uint8_t, 2 * SIZE - 1> diag2 = {};
    return placeQueen(positions, rows, diag1, diag2, 0);
  }
  void printSolution(vector<position> positions) {
    array<bool, SIZE * SIZE> board = {false};
    for (auto p : positions) {
      auto index = SIZE * p[1] + p[0];
      board[index] = true;
    }
    cout << setw(SIZE + 2) << setfill('-') << "";

    cout << positions << endl;
    for (int j = 0; j < SIZE; ++j) {
      string ss;
      ss.reserve(SIZE + 2);
      ss += '|';
      for (int i = 0; i < SIZE; ++i) {
        char state = board[SIZE * j + i] ? 'Q' : '*';
        ss += state;
      }
      ss += '|';
      cout << ss << endl;
    }
    cout << setw(SIZE + 2) << setfill('-') << "" << endl;
  }

};

void testDiagFunction() {
  PlaceQueens<4> solution;
  assert(solution.getDiagLR(0, 0) == 0);
  assert(solution.getDiagLR(1, 0) == 1);
  assert(solution.getDiagLR(2, 0) == 2);
  assert(solution.getDiagLR(3, 0) == 3);

  assert(solution.getDiagLR(0, 1) == 1);
  assert(solution.getDiagLR(1, 1) == 2);
  assert(solution.getDiagLR(2, 1) == 3);
  assert(solution.getDiagLR(3, 1) == 4);

  assert(solution.getDiagLR(0, 2) == 2);
  assert(solution.getDiagLR(1, 2) == 3);
  assert(solution.getDiagLR(2, 2) == 4);
  assert(solution.getDiagLR(3, 2) == 5);

  assert(solution.getDiagLR(0, 3) == 3);
  assert(solution.getDiagLR(1, 3) == 4);
  assert(solution.getDiagLR(2, 3) == 5);
  assert(solution.getDiagLR(3, 3) == 6);

  assert(solution.getDiagRL(0, 0) == 3);
  assert(solution.getDiagRL(1, 0) == 2);
  assert(solution.getDiagRL(2, 0) == 1);
  assert(solution.getDiagRL(3, 0) == 0);

  assert(solution.getDiagRL(0, 1) == 4);
  assert(solution.getDiagRL(1, 1) == 3);
  assert(solution.getDiagRL(2, 1) == 2);
  assert(solution.getDiagRL(3, 1) == 1);

  assert(solution.getDiagRL(0, 2) == 5);
  assert(solution.getDiagRL(1, 2) == 4);
  assert(solution.getDiagRL(2, 2) == 3);
  assert(solution.getDiagRL(3, 2) == 2);

  assert(solution.getDiagRL(0, 3) == 6);
  assert(solution.getDiagRL(1, 3) == 5);
  assert(solution.getDiagRL(2, 3) == 4);
  assert(solution.getDiagRL(3, 3) == 3);
}
int main(int argc, char **argv) {
  testDiagFunction();

  PlaceQueens<BOARD_SIZE> solution;
  auto results = solution.solve();
  if (results.size() < 32) {
    for (auto result: results) {
      solution.printSolution(result);
    }
  } else if (results.size() < 256) {
    for (auto result: results) {
      cout << result << endl;
    }
  } else {
    cout << "Solutions: " << results.size();
  }
}

