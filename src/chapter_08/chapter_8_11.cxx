#include <utility>

#include <unordered_map>

#include <iostream>
#include <string>
#include <array>
#include <vector>
#include <iomanip>
#include <chrono>
#include <cassert>
using namespace std;
typedef array<uint32_t, 6> Wallet;

//static const Wallet COINS = {50, 25, 20, 10, 5, 2, 1};
static const Wallet COINS = {50, 20, 10, 5, 2, 1};

static const uint32_t amount = 245;

class EnumCoins {
 private:
  uint32_t reuse = 0;

  const Wallet &COINS;
  unordered_map<uint64_t, vector<Wallet>> memo;
 public:

  explicit EnumCoins(const Wallet &coins) : COINS(coins) {}

  vector<Wallet> enumerate(uint32_t amount) {
    if (amount == 0) {
      return vector<Wallet>();
    }
    return enumerate(amount, Wallet{0}, 0);
  }
  void printAboutMemoization() {
    cout << "Memoization: " << endl;
    cout << "\telements: " << memo.size() << endl;
    cout << "\treuse: " << reuse << endl;

  }
 private:

  vector<Wallet> enumerate(uint32_t amount,
                           Wallet current,
                           uint32_t index) {
    //TODO Assume there's always a coin of size 1

    if (index == COINS.size() - 1) {
      current[index] = amount;
      return {current};
    }
    if (amount == 0) {
      return {current};
    }
    if (isInMemo(index, amount)) {
      return getFromMemo(index, amount);
    }
    uint32_t value = amount / COINS[index];
    vector<Wallet> results;
    for (auto i = 0u; i <= value; ++i) {
      auto copy = current;
      copy[index] = i;
      auto newAmount = amount - (copy[index] * COINS[index]);
      auto result = enumerate(newAmount, copy, index + 1);
      results.insert(results.end(), result.begin(), result.end());
    }
    setInMemo(index, amount, results);
    return results;
  }
  inline uint64_t static makeKey(const uint32_t index, const uint32_t amount) {
    return ((uint64_t) index << 32) | amount;
  }
  inline bool isInMemo(const uint32_t index, const uint32_t amount) {
    // cout << "?" << index << "," << amount << endl;
    return memo.count(makeKey(index, amount)) > 0;
    //return false;
  }
  inline vector<Wallet> getFromMemo(const uint32_t index, const uint32_t amount) {
    reuse++;
    // cout << "~" << index << ',' << amount << endl;
    return memo[makeKey(index, amount)];
  }
  inline void setInMemo(uint32_t index, uint32_t amount, vector<Wallet> vector) {
    // cout << "#" << index << ',' << amount << " !" << makeKey(index, amount) << endl;
    if (memo.count(makeKey(index, amount)) == 0) {
      memo[makeKey(index, amount)] = std::move(vector);
    } else {
      //  cerr << "Unexpected extra insert of " << index << ',' << amount << " !" << makeKey(index, amount) << endl;
    };

  }
};

void runWallet();
ostream &operator<<(ostream &os, const Wallet &wallet) {
  os << '[';
  for (auto i : wallet) {
    os << setw(3) << right << i;
  }
  os << ']';
  return os;
}

void runWallet(EnumCoins &solution, uint32_t amount) {
  auto start = chrono::system_clock::now();

  auto wallets = solution.enumerate(amount);
  auto finish = chrono::system_clock::now();
  auto time = chrono::duration_cast<chrono::milliseconds>(finish - start).count();
  cout.setstate(ios_base::goodbit);
  cout << ">  " << COINS << " $$ " << amount << endl;
  if (wallets.size() < 256) {
    for (auto i = 0; i < wallets.size(); ++i) {
      auto wallet = wallets[i];
      cout << setw(3) << left << i << wallet << endl;
    }
  }
  cout.imbue(locale(""));
  cout << "Found solutions: " << wallets.size() << ", in: " << time << "ms" << endl;

  solution.printAboutMemoization();
}

int main(int argc, char **argv) {
  // ostream::sync_with_stdio(true);
  // cout.setstate(ios_base::badbit);
  // cerr.setstate(ios_base::badbit);
  EnumCoins solution(COINS);
  runWallet(solution, amount);
  // 
  runWallet(solution, amount - 1);
  runWallet(solution, 21);

}
