//
// Created by crypt on 6/17/19.
//
#include <unordered_map>
#include <algorithm>
#ifndef GOOGLE_ME_UP_TRIE_HXX
#define GOOGLE_ME_UP_TRIE_HXX
class trie;

class trie {
 public:
  std::string finalWord;
  std::unordered_map<char, trie *> *root;

  trie() {
    root = new std::unordered_map<char, trie *>();
  }
  ~trie() {
    std::for_each(root->begin(), root->end(),
                  [](auto &item) {
                    trie *t = item.second;
                    delete t;
                  }
    );
    root->clear();
    delete root;
  }
  bool insert(const std::string &word) {
    auto prev = this;
    for (auto c : word) {
      auto *current = prev->root;
      if (current->count(c) > 0) {
        prev = current->at(c);
      } else {
        prev = new trie;
        current->insert({c, prev});
      }
    }
    if (prev->finalWord.empty()) {
      prev->finalWord = word;
      return true;
    }
    return false;
  }
  const trie *getNext(const char c) const {
    return this->root->at(c);
  }
  const bool contains(const char c) const {
    return this->root->count(c);
  }
  const bool hasFinalWord() const {
    return !this->finalWord.empty();
  }

  friend
  std::ostream &operator<<(std::ostream &out,
                           const trie &trie);
};
std::ostream &operator<<(std::ostream &out,
                         const trie &t) {
  out << (t.finalWord.empty() ? "_" : t.finalWord) << '[';
  for_each(t.root->begin(), t.root->end(),
           [&out](auto &tuple) {
             out  << tuple.first << ',';
           });
  out << ']';
  return out;
}
#endif //GOOGLE_ME_UP_TRIE_HXX
