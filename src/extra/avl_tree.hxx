//
// Created by Francesco Fiduccia on 2019-03-14.
//

#ifndef GOOGLE_ME_UP_SRC_EXTRA_AVL_TREE_HXX_
#define GOOGLE_ME_UP_SRC_EXTRA_AVL_TREE_HXX_
#include <sstream>
#include <iostream>

using std::ostream;

template<typename T>
class AVLNode;

template<typename T>
class AVLTree {
  enum Side {
    LEFT,
    RIGHT
  };
 private:
  uint32_t size;
  AVLNode<T> *root;
  uint32_t internal_insert(AVLNode<T> *current, AVLNode<T> *node);
 public:
  AVLTree();
  void insert(T value);
/*
 * bool contains(T value);

  void min(T value);
  void max(T value);
  void remove(T value);
  */

  void print(ostream &out);
  T getNext(T value);
  T getPrevious(T value);

 private:
  AVLNode<T> *getNextNode(AVLNode<T> *node);
  AVLNode<T> *getPrevNode(AVLNode<T> *node);
  AVLNode<T> *findNode(T value);

  void rotateLeftLeft(AVLNode<T> *node);
  void rotateLeftRight(AVLNode<T> *node);

  void rotateRightRight(AVLNode<T> *node);
  void rotateRightLeft(AVLNode<T> *node);
  void assertTree() const;
  void check(AVLNode<T> *node, Side side) const;
  void updateNodeHeight(AVLNode<T> *node);
  int getHeight(const AVLNode<T> *current) const;
};
#include "avl_tree.txx"

#endif // GOOGLE_ME_UP_SRC_EXTRA_AVL_TREE_HXX_
