//
// Created by Francesco Fiduccia on 2019-03-14.
//
#include <string>
#include <iostream>
#include <list>
#include <stack>
#include <sstream>
#include <iomanip>
#include <algorithm>

#include "avl_tree.hxx"

using std::list;
using std::string;
using std::stack;
using std::stringstream;
using std::endl;
using std::queue;
using std::setw;
using std::setfill;
using std::flush;
using std::max;

template<typename T>
class AVLNode {
  friend class AVLTree<T>;
  AVLNode<T> *parent;
  AVLNode<T> *left;
  AVLNode<T> *right;
  T value;
  uint32_t height;
 public:
  explicit AVLNode(T value) :
      value(value),
      parent(nullptr), left(nullptr), right(nullptr),
      height(1) {

  }
  string to_print() {
    return std::to_string(value);
  }
};
template<typename T>
AVLTree<T>::AVLTree() : root(nullptr) {

}
template<typename T>
void AVLTree<T>::print(ostream &out) {
  list<list<AVLNode<T> *> > output;
  list<AVLNode<T> *> workQueue;
  bool notEmpty = false;
  if (root != nullptr) {
    workQueue.emplace_back(root);
    list<AVLNode<T> *> rootList{root};
    output.emplace_back(rootList);
    notEmpty = true;
  }
  uint32_t valueLength = 0;
  uint32_t maxValue = 0;
  while (notEmpty) {
    notEmpty = false;
    list<AVLNode<T> *> nextQueue;
    list<AVLNode<T> *> outputLine;
    // stringstream &ss == ;
    // output.push_back(ss);
    while (!workQueue.empty()) {
      auto node = workQueue.front();
      workQueue.pop_front();
      if (node != nullptr && node->left) {
        if (maxValue < node->left->value) {
          valueLength = std::max(valueLength, (uint32_t) log10(node->left->value));
          maxValue = node->left->value;
        }
        nextQueue.emplace_back(node->left);
        outputLine.emplace_back(node->left);
        notEmpty = true;
      } else {
        nextQueue.emplace_back(nullptr);
        outputLine.emplace_back(nullptr);
      }
      if (node != nullptr && node->right) {
        if (maxValue < node->right->value) {
          valueLength = std::max(valueLength, (uint32_t) log10(node->right->value));
          maxValue = node->right->value;
        }
        nextQueue.emplace_back(node->right);
        outputLine.emplace_back(node->right);
        notEmpty = true;
      } else {
        nextQueue.emplace_back(nullptr);
        outputLine.emplace_back(nullptr);
      }
    }
    if (notEmpty) {
      output.push_back(outputLine);
    }
    outputLine.clear();
    workQueue = nextQueue;
  }
  // auto empty_space = (((output.size() - countLine) * 4) / 2);
  out << setw(pow(2, output.size()) * 2 + 7) << setfill('*') << '*' << endl;
  out << setfill(' ') << setw(0);
  const uint32_t k = 1u;
  uint32_t countLine = output.size() - 1;
  valueLength += 2;
  for (auto &line : output) {

    uint32_t offset = (pow(2, countLine) * (k));

    // << setw(empty_space) << ' ' << setw(0);


    out << setw(3) << countLine << ' ' << setw(3) << offset << ' ' << setw(3) << offset * 2 << " *";
    //out << setw(offset);
    uint32_t countElement = 0;

    for (const auto element : line) {
      /*if (countElement == pow( countLine-1,2)){
        out << setw(offset - 2 ) << setfill('_') << "" <<setw(0) << setfill(' ');
      }
       */
      if (element) {
        out << setw(offset + valueLength) << element->value << '@' << element->height;
      } else {
        out << setw(offset + valueLength) << '.';
      }
      if (countElement == 0) {
        offset += offset;
      }
      countElement--;
    }
    out << setw(0);
    //empty_space -= 2;
    countLine--;
    out << endl;
  }
  out << setw(pow(2, output.size()) * 2 + 7) << setfill('*') << '*' << endl;
  out << setfill(' ') << setw(0);
}

template<typename T>
uint32_t AVLTree<T>::internal_insert(AVLNode<T> *current, AVLNode<T> *node) {

  if (current->value > node->value) {
    if (current->left) {
      current->height =
          1 + std::max(internal_insert(current->left, node), current->right != nullptr ? current->right->height : 0);
    } else {
      current->left = node;
      node->parent = current;
      if (current->right == nullptr) {
        current->height++;
      }
    }
    return current->height;
  } else {
    if (current->right) {
      current->height =
          1 + std::max(internal_insert(current->right, node), current->left != nullptr ? current->left->height : 0);
    } else {
      current->right = node;
      node->parent = current;
      if (current->left == nullptr) {
        current->height++;
      }
    }
    return current->height;
  }
}

template<typename T>
T AVLTree<T>::getPrevious(T value) {
  AVLNode<T> *current = this->findNode(value);
  const auto prevNode = this->getPrevNode(current);
  return prevNode != nullptr ? prevNode->value : value;
}

template<typename T>
T AVLTree<T>::getNext(T value) {
  AVLNode<T> *current = this->findNode(value);
  const auto nextNode = this->getNextNode(current);
  return nextNode != nullptr ? nextNode->value : value;
}

template<typename T>
void AVLTree<T>::insert(T value) {
  this->assertTree();
  this->size++;
  auto node = new AVLNode(value);
  if (this->root == nullptr) {
    root = node;
    return;
  }
  // TODO check this height
  root->height = internal_insert(this->root, node);
  auto current = node;
  bool updateHeight = false;
  while (current != nullptr && current->parent != nullptr) {
    Side parentSide = current->parent->left == current ? LEFT : RIGHT;

    // a parent must exist since otherwise current would have been root !
    auto grandParent = current->parent->parent;

    if (grandParent != nullptr) {
      Side grandParentSide = grandParent->left == current->parent ? LEFT : RIGHT;

      // AVLNode<T> *uncle = grandParentSide == LEFT ? grandParent->right : grandParent->left;
      // auto uncleHeight = uncle != nullptr ? uncle->height : 0;
      int balance = getHeight(grandParent->left) - getHeight(grandParent->right);
      if (balance < -1 || balance > 1) {
        this->print(std::cout);
        updateHeight = true;
        if (parentSide == LEFT && grandParentSide == LEFT) {
          this->rotateRightRight(current);
        } else if (parentSide == RIGHT && grandParentSide == RIGHT) {
          this->rotateLeftLeft(current);
        } else if (parentSide == LEFT && grandParentSide == RIGHT) {
          this->rotateRightLeft(current);
        } else {
          this->rotateLeftRight(current);
        }
      }
    }
    current = current->parent;
    auto tmp = current;
    while (updateHeight && tmp != nullptr) {
      this->updateNodeHeight(tmp);
      tmp = tmp->parent;
    }
  }
  // this->assertTree();
}
template<typename T>
int AVLTree<T>::getHeight(const AVLNode<T> *current) const {
  return current != nullptr ? current->height : 0;
}
template<typename T>
void AVLTree<T>::updateNodeHeight(AVLNode<T> *node) {
  node->height = 1 + std::max(node->right != nullptr ? node->right->height : 0,
                              node->left != nullptr ? node->left->height : 0);
}

template<typename T>
AVLNode<T> *AVLTree<T>::getPrevNode(AVLNode<T> *node) {
  if (node == nullptr) {
    return nullptr;
  }
  // Direct left !
  if (node->left != nullptr) {
    auto current = node->left;
    while (current->right != nullptr) {
      current = current->right;
    }
    return current;
  } else if (node->parent != nullptr) {
    // Check Parent
    auto current = node;
    // stop before the first parent on our left (has the node on the right)
    // or if on the root node
    while (current != nullptr
        && current->parent != nullptr
        && current->parent->left == current) {
      current = current->parent;
    }
    if (current != nullptr
        && current->parent != nullptr
        && current->parent->right == current) {
      return current->parent;
    }
  }
  return nullptr;
}

template<typename T>
AVLNode<T> *AVLTree<T>::getNextNode(AVLNode<T> *node) {
  if (node == nullptr) {
    return nullptr;
  }
  // Direct right !
  if (node->right != nullptr) {
    auto current = node->right;
    while (current->left != nullptr) {
      current = current->left;
    }
    return current;
  } else if (node->parent != nullptr) {
    // Check Parent
    auto current = node;
    // stop before the first parent on our right (has the node on the left)
    // or if on the root node
    while (current != nullptr
        && current->parent != nullptr
        && current->parent->right == current) {
      current = current->parent;
    }
    if (current != nullptr
        && current->parent != nullptr
        && current->parent->left == current) {
      return current->parent;
    }
  }
  return nullptr;
}

template<typename T>
AVLNode<T> *AVLTree<T>::findNode(T value) {
  auto current = root;
  while (value != current->value) {
    if (current->value > value) {
      current = current->left;
    } else {
      current = current->right;
    }
  }
  return (current->value == value) ? current : nullptr;
}

template<typename T>
void AVLTree<T>::rotateLeftRight(AVLNode<T> *node) {
  auto parent = node->parent;
  auto grandParent = parent->parent;
  std::cout << "rotateLeftRight -> " << node->to_print()
            << ' ' << parent->to_print()
            << ' ' << grandParent->to_print()
            << endl;

  this->assertTree();
  auto tempRoot = grandParent->parent;

  if (tempRoot == nullptr) {
    root = node;
    node->parent = nullptr;
  } else if (tempRoot->left == grandParent) {
    tempRoot->left = node;
  } else if (tempRoot->right == grandParent) {
    tempRoot->right = node;
  }
  node->parent = tempRoot;

  auto t2 = node->left;
  auto t3 = node->right;

  node->right = grandParent;
  grandParent->parent = node;
  node->left = parent;
  parent->parent = node;

  parent->right = t2;
  if (t2 != nullptr) t2->parent = parent;

  grandParent->left = t3;
  if (t3 != nullptr) t3->parent = grandParent;

  updateNodeHeight(parent);
  updateNodeHeight(grandParent);
  updateNodeHeight(node);
  this->assertTree();

}

template<typename T>
void AVLTree<T>::rotateRightLeft(AVLNode<T> *node) {
  auto parent = node->parent;
  auto grandParent = parent->parent;
  std::cout << "rotateRightLeft -> " << node->to_print()
            << ' ' << parent->to_print()
            << ' ' << grandParent->to_print()
            << endl;
  this->assertTree();
  auto tempRoot = grandParent->parent;

  if (tempRoot == nullptr) {
    root = node;
    node->parent = nullptr;
  } else if (tempRoot->left == grandParent) {
    tempRoot->left = node;
  } else if (tempRoot->right == grandParent) {
    tempRoot->right = node;
  }
  node->parent = tempRoot;

  auto t2 = node->left;
  auto t3 = node->right;

  node->left = grandParent;
  grandParent->parent = node;
  node->right = parent;
  parent->parent = node;

  grandParent->right = t2;
  if (t2 != nullptr) t2->parent = grandParent;

  parent->left = t3;
  if (t3 != nullptr) t3->parent = parent;

  updateNodeHeight(parent);
  updateNodeHeight(grandParent);
  updateNodeHeight(node);
  this->assertTree();

}

template<typename T>
void AVLTree<T>::rotateRightRight(AVLNode<T> *node) {
  auto parent = node->parent;
  auto grandParent = parent->parent;
  std::cout << "rotateRightRight -> " << node->to_print()
            << ' ' << parent->to_print()
            << ' ' << grandParent->to_print()
            << endl;
  this->assertTree();

  auto t3 = parent->right;
  auto tempRoot = grandParent->parent;
  parent->right = grandParent;
  grandParent->parent = parent;
  grandParent->left = t3;
  if (t3 != nullptr) t3->parent = grandParent;
  if (tempRoot == nullptr) {
    root = parent;
    parent->parent = nullptr;
  } else if (tempRoot->left == grandParent) {
    tempRoot->left = parent;
  } else if (tempRoot->right == grandParent) {
    tempRoot->right = parent;
  }
  parent->parent = tempRoot;

  // Height adjustments
  this->updateNodeHeight(grandParent);
  this->updateNodeHeight(parent);
  this->assertTree();
}

template<typename T>
void AVLTree<T>::rotateLeftLeft(AVLNode<T> *node) {
  auto parent = node->parent;
  auto grandParent = parent->parent;
  std::cout << "rotateLeftLeft -> "
            << node->to_print()
            << ' ' << parent->to_print()
            << ' ' << grandParent->to_print()
            << endl;
  this->assertTree();
  auto t2 = parent->left;
  auto tempRoot = grandParent->parent;
  parent->left = grandParent;
  grandParent->parent = parent;
  grandParent->right = t2;
  if (t2 != nullptr) t2->parent = grandParent;
  if (tempRoot == nullptr) {
    root = parent;
    parent->parent = nullptr;
  } else if (tempRoot->right == grandParent) {
    tempRoot->right = parent;
  } else if (tempRoot->left == grandParent) {
    tempRoot->left = parent;
  }
  parent->parent = tempRoot;

  // Height adjustments
  this->updateNodeHeight(grandParent);
  this->updateNodeHeight(parent);
  this->assertTree();
}
template<typename T>
void AVLTree<T>::check(AVLNode<T> *node, Side side) const {
  if (node == nullptr) {
    return;
  }

  auto child = side == LEFT ? node->left : node->right;
  auto sideText = side == LEFT ? "SIDE" : "RIGHT";
  if (child == nullptr) {
    return;
  }

  if (child->parent != node) {
    std::cout << "ERROR in tree's PARENT - " << sideText << " " << node->value << "->" << child->value << endl;
  }
  if (child->height >= node->height) {
    std::cout << "ERROR in tree's HEIGHT - " << sideText << " " << node->value << "->" << child->value << endl;
  }
  if ((side == LEFT && child->value >= node->value) || (side == RIGHT && child->value <= node->value)) {
    std::cout << "ERROR in tree's VALUE - " << sideText << " " << node->value << "->" << child->value << endl;
  }
}

template<typename T>
void AVLTree<T>::assertTree() const {
  // std::cout << "AssertTree";
  std::deque<AVLNode<T> *> workQueue1;
  std::deque<AVLNode<T> *> workQueue2;
  if (this->root) {
    workQueue1.push_back(this->root);
  }
  auto current = &workQueue1;
  auto next = &workQueue2;
  while (!current->empty()) {
    do {
      auto node = current->front();
      current->pop_front();
      // ENABLE FOR DEBUG
      // std::cout << ' ' << node->to_print();
      if (node->left != nullptr) {
        check(node, LEFT);
        next->push_back(node->left);
      }
      if (node->right != nullptr) {
        check(node, RIGHT);
        next->push_back(node->right);
      }
    } while (!current->empty());
    auto temp = current;
    current = next;
    next = temp;
    next->clear();
  }
  // std::cout << endl;
}
