#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <list>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};


template<typename T>
ostream &operator<<(ostream &out, const list<T> &list) {
    for (const auto &element: list) {
        out << element << ' ';
    }
    return out;
}

template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T>
ostream &operator<<(ostream &out, const pair<T, T> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

enum Operator {
    plus, minus, times, divide, none
};

unordered_map<char, Operator> operator_matcher = {
        {'+', Operator::plus},
        {'-', Operator::minus},
        {'/', Operator::divide},
        {'*', Operator::times}
};
unordered_map<Operator, char> operator_tostring = {
        {Operator::plus,   '+'},
        {Operator::minus,  '-'},
        {Operator::divide, '/'},
        {Operator::times,  '*'},
        {Operator::none,   ' '}
};

class Node {
public:

    Operator op;
    double number;

    Node(Operator op) : op(op), number(NAN) {}

    Node(double number) : op(none), number(number) {}
};


ostream &operator<<(ostream &out, const Node &node) {
    if (node.op == Operator::none) {
        out << node.number;
    } else {
        out << operator_tostring[node.op];
    }
    return out;
}

class Calculator {
public:

private:
    //vector<size_t> priorities;
    //vector<Node> nodes;
    vector<list<Node>::iterator> priorities;
    list<Node> nodes;

public:

    double solve(const string &input) {

        parse_input(input);
        for (auto it : priorities) {
            auto next = it;
            auto prev = it;
            prev--;
            next++;
            auto &v1 = *prev;
            auto &v2 = *next;
            auto &node = *it;
            double result;
            if (node.op == times) {
                result = v1.number * v2.number;
            } else {
                result = v1.number / v2.number;
            }
            cout << v1 << node << v2 << " -> ";
            nodes.insert(prev, Node(result));
            nodes.erase(prev, ++next);
            cout << nodes << endl;
        }
        stack<double> operands;
        Operator op = none;
        for (Node &node : nodes) {
            if (node.op == none) {
                operands.emplace(node.number);
            } else {
                op = node.op;
            }
            if (operands.size() == 2 && op != none) {
                auto p2 = operands.top();
                operands.pop();
                auto p1 = operands.top();
                operands.pop();
                cout << p1 << operator_tostring[op] << p2 << endl;
                if (op == Operator::plus) {
                    operands.push(p1 + p2);
                } else {
                    operands.push(p1 - p2);
                }
            }
        }
        return operands.top();
    }

    void parse_input(const string &input) {
        auto current = 0;
        auto index = input.find_first_of("+-*/", current);
        while (index != -1) {
            string ser = input.substr(current, index - current);
            cout << ser << endl;
            size_t next;
            double number = stod(ser);
            Operator op = operator_matcher[input[index]];

            nodes.emplace_back(number);
            auto it = nodes.insert(nodes.end(), op);
            if (op == times || op == divide) {
                priorities.emplace_back(it);
            }

            current = index + 1;
            index = input.find_first_of("+-*/", current);
        }
        string ser = input.substr(current);
        double number = stod(ser);
        nodes.emplace_back(number);
        cout << number << endl;
        cout << nodes << endl;
    }
};

void check(const string &input, const double expected) {
    Calculator calc;
    auto actual = calc.solve(input);
    cout << (actual == expected ? '=' : '!') << ' ' << input << ' ' << actual << ' ' << expected << endl;
}

int main(int argc, char **argv) {
    string input = "2*3+5/6*3+15";
    double output = 23.5;
    string input2 = "5/6*3+15+2*3";
    double output2 = 23.5;
    string input3 = "5*3/6+3*2+15";
    double output3 = 23.5;
    check(input, output);
    check(input2, output2);
    check(input3, output3);

}