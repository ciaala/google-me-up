#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <array>
#include <iomanip>
#include <unordered_map>
#include <unordered_set>

using namespace std;
template<size_t SIZE>
using Board = array<array<char, SIZE>, SIZE>;


class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<size_t SIZE>
class TicTacToe {
private:
    string found;
    static const char p1 = 'x';

    unordered_set<string> discarded;
    unordered_map<string, uint32_t> paths_p1;
    unordered_map<string, uint32_t> paths_p2;

    bool update_(string solutionId, char c) {
        auto &paths = c == p1 ? paths_p1 : paths_p2;
        auto &other = c == p1 ? paths_p2 : paths_p1;
        if (discarded.count(solutionId) > 0) {
            return false;
        }
        if (paths.count(solutionId) > 0) {
            paths[solutionId]++;
            if (paths[solutionId] == SIZE) {
                this->found = solutionId;
                return true;
            }
        } else if (other.count(solutionId) > 0) {
            discarded.emplace(solutionId);
            other.erase(solutionId);
        } else {
            paths[solutionId] = 1;
        }
        return false;
    }

public:
    char whichPlayerWon(const Board<SIZE> &board) {
        this->discarded.clear();
        this->paths_p1.clear();
        this->paths_p2.clear();

        for (uint32_t j = 0; j < SIZE; j++) {
            auto &row = board[j];
            for (uint32_t i = 0; i < SIZE; i++) {
                char c = row[i];
                if (c != ' ') {
                    if ((i - j == 0) && update_("left_diag", c)) return c;
                    if (((i + j) == SIZE - 1) && update_("right_diag", c)) return c;
                    if (update_(string("col_") + to_string(i), c)) return c;
                    if (update_(string("row_") + to_string(j), c)) return c;
                }
            }
        }
        return ' ';
    }

    const string &getFound() {
        return this->found;
    }
};

template<size_t SIZE>
ostream &operator<<(ostream &out, const array<char, SIZE> &row) {

    for (auto &cell : row) {
        out << setw(3) << cell;
    }
    return out;
}

template<size_t SIZE>
ostream &operator<<(ostream &out, const Board<SIZE> &board) {
    static const char BOARD_CHAR = '#';
    int length = static_cast<int>((board.size()) * 3 + 4);
    out << setfill(BOARD_CHAR) << setw(length) << BOARD_CHAR << endl << setfill(' ');
    out << BOARD_CHAR << setw(length - 1) << BOARD_CHAR << endl << setfill(' ');


    for (auto &row : board) {
        out << BOARD_CHAR << row << "  " << BOARD_CHAR << endl;
    }
    out << BOARD_CHAR << setw(length - 1) << BOARD_CHAR << endl << setfill(' ');

    out << setfill(BOARD_CHAR) << setw(length) << BOARD_CHAR << endl << setfill(' ');

    return out;
}

template<size_t SIZE>
void lookForWinner(Board<SIZE> &board, char expectedWinner, string expectedCombo) {
    cout << board << endl;
    TicTacToe<SIZE> solution;
    auto whichPlayerWon = solution.whichPlayerWon(board);

    if (whichPlayerWon != expectedWinner || expectedCombo != solution.getFound()) {
        cout.flush();
        cerr << "Expected Winner: " << expectedWinner << '@' << expectedCombo << endl;
    }
    if (whichPlayerWon != ' ') {
        cout << "Winner: " << whichPlayerWon << " " << solution.getFound() << endl;
    }
}

int main(int argc, char **argv) {
    {
        Board<3> board = {{
                                  {'x', 'o', 'o'},
                                  {'x', ' ', ' '},
                                  {'x', ' ', ' '}}};
        lookForWinner(board, 'x', "col_0");
    }
    {
        Board<3> board = {{
                                  {'o', 'o', 'o'},
                                  {'x', ' ', ' '},
                                  {'x', ' ', ' '}}};
        lookForWinner(board, 'o', "row_0");
    }
    {
        Board<3> board = {{
                                  {'x', 'o', 'o'},
                                  {'x', 'x', ' '},
                                  {'o', ' ', 'x'}}};
        lookForWinner(board, 'x', "left_diag");
    }
    {
        Board<3> board = {{
                                  {' ', 'x', ' '},
                                  {' ', 'x', ' '},
                                  {' ', 'x', ' '}}};
        lookForWinner(board, 'x', "col_1");
    }
    {
        Board<3> board = {{
                                  {' ', ' ', 'x'},
                                  {' ', 'x', ' '},
                                  {'x', ' ', ' '}}};
        lookForWinner(board, 'x', "right_diag");
    }
    {
        Board<3> board = {{
                                  {'x', ' ', ' '},
                                  {' ', 'x', ' '},
                                  {' ', ' ', 'x'}}};
        lookForWinner(board, 'x', "left_diag");
    }

    {
        Board<3> board = {{
                                  {'o', ' ', ' '},
                                  {' ', 'o', ' '},
                                  {' ', ' ', 'o'}}};
        lookForWinner(board, 'o', "left_diag");
    }
    {
        Board<3> board = {{
                                  {' ', ' ', 'o'},
                                  {' ', 'o', ' '},
                                  {'o', ' ', ' '}}};
        lookForWinner(board, 'o', "right_diag");
    }
}