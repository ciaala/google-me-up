#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename T>
ostream &operator<<(ostream &out, const pair<T, T> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << '@' << result.size() << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

class PairOfIntegersSum {
public:
    vector<pair<int, int >> solve(vector<int> numbers, const int sum) {
        unordered_map<int, size_t> cache;
        vector<pair<int, int >> solution;
        for (const int &number : numbers) {
            auto diff = sum - number;
            if (cache.count(diff) > 0) {
                solution.emplace_back(number, diff);
                auto count = cache[diff];
                count--;
                if (count == 0) {
                    cache.erase(diff);
                } else {
                    cache[diff] = count;
                }
            } else {
                auto count = cache.count(number) ? cache[number] + 1 : 1;
                cache[number] = count;
            }
        }
        return solution;
    }
};

int main(int argc, char **argv) {
    Dice dice;
    auto SIZE = 200;
    vector<int> numbers(SIZE);
    for (int i = 0u; i < SIZE; i++) {
        numbers[i] = dice.next(-100, 100);
    }
    PairOfIntegersSum pair;
    int guess = dice.next(-100, 100);
    auto solution = pair.solve(numbers, guess);
    cout << guess << endl << solution << endl;

    solution = pair.solve({1,3,3,7,7,7,3,9,3}, 10);
    cout << 10 << endl << solution << endl;

}