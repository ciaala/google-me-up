//
// Created by Francesco Fiduccia on 2019-01-20.
//
#include <cstdlib>
#include <stdlib.h>
#include <iostream>



using namespace std;

template<typename T>
void swapNumber(T &a, T &b) {
    a = a ^ b;
    b = b ^ a;
    a = a ^ b;
}
void swapNumber(double &a, double &b) {
    auto &aa= reinterpret_cast<uint64_t &>(a);
    auto &bb= reinterpret_cast<uint64_t &>(b);
    swapNumber(aa,bb);
}

template<typename T>
void showNumberSwapping(T a, T b) {
    cout << a << ',' << b << endl;
    swapNumber(a, b);
    cout << a << ',' << b << endl;
}
int main(int argc, char**argv) {
    showNumberSwapping(12, 12310);
    showNumberSwapping(999.3, 123.3);
    showNumberSwapping('a', 'c');
    return 0;
}