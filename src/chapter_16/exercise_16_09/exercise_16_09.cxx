#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class AllAddCalculator {

    int negate(int number) {
        auto inc = number > 0 ? -1 : 1;
        auto result = 0;
        while (number != 0) {
            number += inc;
            result += inc;
        }
        return result;
    }

public:
    int add(int first, int second) {
        return first + second;
    }

    int subtract(int first, int second) {

        auto sign_first = first >= 0 ? 1 : -1;
        auto sign_second = second >= 0 ? 1 : -1;
        int inc;
        if (first > second) {
            inc = +1;
        } else {
            inc = -1;
        }

        auto result = 0u;
        while (first != second + result) {
            result += inc;
        }
        return result;
    }

    int divide(int first, int second) {
        if (second == 0) {
            return NAN;
        }
        auto sign_first = first >= 0 ? 1 : -1;
        auto sign_second = second >= 0 ? 1 : -1;
        int dividend;
        int inc;
        if (sign_first != sign_second) {
            dividend = negate(second);
            inc = -1;
        } else {
            dividend = second;
            inc = 1;
        }
        auto result = 0, temp = dividend;
        while (abs(temp) <= abs(first)) {
            temp += dividend;
            result += inc;
        }
        return result;
    }

    int multiply(int first, int second) {

        if (abs(first) < abs(second)) {
            return multiply(second, first);
        }
        // skip if operand is 0
        if (first == 0 || second == 0) {
            return 0;
        }
        if (first == 1) return second;
        if (second == 1) return first;

        auto sign_first = first >= 0 ? 1 : -1;
        auto sign_second = second >= 0 ? 1 : -1;


        int base;
        int inc;
        int count;
        if (sign_first != sign_second) {
            base = sign_first < 0 ? first : second;
            count = sign_first < 0 ? second : first;
            // count is positive
            inc = -1;
        } else if (sign_first < 0 && sign_second < 0) {
            base = negate(first);
            count = second;
            // count is negative
            inc = 1;
        } else {
            base = first;
            count = second;
            inc = -1;
        }
        auto result = 0;
        while (count != 0) {
            count += inc;
            result += base;
        }
        return result;
    }
};

void check_multiply(int first, int second, int expected) {
    AllAddCalculator calculator;
    auto result = calculator.multiply(first, second);
    cout << first << " x " << second << " => " << result << ": " << expected << (expected == result ? '!' : '?')
         << endl;
}

void check_division(int first, int second, int expected) {
    AllAddCalculator calculator;
    auto result = calculator.divide(first, second);
    cout << first << " / " << second << " => " << result << ": " << expected << (expected == result ? '!' : '?')
         << endl;
}


void check_subtraction(int first, int second, int expected) {
    AllAddCalculator calculator;
    auto result = calculator.subtract(first, second);
    expected = first - second;
    cout << first << " - " << second << " => " << result << ": " << expected << (expected == result ? '!' : '?')
         << endl;
}

int main(int argc, char **argv) {
    check_multiply(2, 1, 2);
    check_multiply(1, 2, 2);
    check_multiply(0, 0, 0);
    check_multiply(0, 2, 0);
    check_multiply(2, 0, 0);
    check_multiply(1, 1, 1);
    check_multiply(1, -1, -1);
    check_multiply(-1, -1, 1);
    check_multiply(3, 4, 12);
    check_multiply(4, 3, 12);
    check_multiply(-4, -3, 12);
    check_multiply(-3, -4, 12);
    check_multiply(-3, 4, -12);
    check_multiply(3, -4, -12);
    check_multiply(3000, -4000, -12000000);
    check_multiply(3000, 4000, 12000000);
    check_multiply(-3000, -4000, 12000000);


    check_division(0, 0, NAN);
    check_division(0, 1, 0);
    check_division(1, 0, NAN);
    check_division(0, 10, 0);
    check_division(20, 10, 2);
    check_division(9, 10, 0);
    check_division(11, 10, 1);
    check_division(110, 10, 11);
    check_division(11, -10, -1);
    check_division(45, -10, -4);
    check_division(-45, -10, 4);
    check_division(-45, 10, -4);

    check_subtraction(0, 0, 0);
    check_subtraction(1, 0, 1);
    check_subtraction(0, 1, -1);
    check_subtraction(0, -1, 1);
    check_subtraction(1, -1, 2);
    check_subtraction(-1, 1, -2);
    check_subtraction(10, 1, 9);
    check_subtraction(1, 10, -9);
    check_subtraction(-1, -10, 9);
    check_subtraction(0, -10, 10);
    check_subtraction(1, -10, 11);
}