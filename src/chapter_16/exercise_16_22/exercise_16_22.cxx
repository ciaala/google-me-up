#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

enum direction {
    left, right, up, down
};

struct Position2D {
    size_t sizeField;
    long i;
    long j;
    direction d;

    Position2D(size_t sizeField) : sizeField(sizeField) {
        i = 0;
        j = 0;
        d = direction::right;
    }

    bool operator==(size_t &value) {
        return value == sizeField * j + i;
    }

    explicit operator uint32_t() const {
        return sizeField * j + i;
    }
};


class hash_pair_long_long {
    const hash<long> ah = hash<long>();
public:
    size_t operator()(const pair<long, long> &key) const {
        size_t seed = ah(key.first);
        return ah(key.first) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    }
};

class LangtonAnt {
    unordered_set<pair<long, long>, hash_pair_long_long> grid;

    size_t size;
    Position2D ant_cell;
    long grid_max_i;
    long grid_min_i;
    long grid_max_j;
    long grid_min_j;

private:
    // 90' left
    unordered_map<direction, direction> turn_w{
            {direction::left,  direction::down},
            {direction::right, direction::up},
            {direction::up,    direction::left},
            {direction::down,  direction::right}
    };
    // 90' right

    unordered_map<direction, direction> turn_b{
            {direction::left,  direction::up},
            {direction::right, direction::down},
            {direction::up,    direction::right},
            {direction::down,  direction::left}
    };

public:

    LangtonAnt() : ant_cell(SIZE_T_MAX) {
        grid_max_i = LONG_MIN;
        grid_min_i = LONG_MAX;
        grid_max_j = LONG_MIN;
        grid_min_j = LONG_MAX;
    }

    void printKMoves(int k) {
        size = k;

        move_ant();

        for (auto i = 0u; i < k; i++) {

            auto cell = this->grid.count({ant_cell.i, ant_cell.j}) > 0 ? 'w' : 'b';
            if (cell == 'w') {
                cell = 'b';
                ant_cell.d = turn_w[ant_cell.d];
            } else if (cell == 'b') {
                cell = 'w';
                ant_cell.d = turn_b[ant_cell.d];
            }
            updateGrid(cell);
            move_ant();

            // printGrid();
        }
    }

    void updateGrid(char cell) {
        this->grid_max_i = max(ant_cell.i, grid_max_i);
        this->grid_min_i = min(ant_cell.i, grid_min_i);
        this->grid_max_j = max(ant_cell.j, grid_max_j);
        this->grid_min_j = min(ant_cell.j, grid_min_j);

        if (cell == 'b') {
            grid.erase({ant_cell.i, ant_cell.j});
        } else {
            grid.insert({ant_cell.i, ant_cell.j});
        }
    }

    void move_ant() {
        switch (this->ant_cell.d) {
            case direction::up: {
                this->ant_cell.j -= 1;
                break;
            }
            case direction::down: {
                this->ant_cell.j += 1;
                break;
            }
            case direction::left: {
                this->ant_cell.i -= 1;
                break;
            }
            case direction::right: {
                this->ant_cell.i += 1;
            }
        }
    }

    void printGrid() {
        auto diff = 1 + grid_max_j - grid_min_j;
        cout << setfill('=') << setw(diff) << '=' << endl;
        for (auto j = grid_min_j; j <= grid_max_j; j++) {

            for (auto i = grid_min_i; i <= grid_max_i; i++) {
                if (ant_cell.i == i && ant_cell.j == j) {
                    cout << 'A';
                } else {
                    cout << ((grid.count({i, j}) > 0) ? 'X' : ' ');
                }
            }
            cout << endl;
        }
        cout << setfill('=') << setw(diff) << '=' << endl;
    }

};

int main(int argc, char **argv) {
    LangtonAnt langton;
    langton.printKMoves(16000);
    langton.printGrid();
}

