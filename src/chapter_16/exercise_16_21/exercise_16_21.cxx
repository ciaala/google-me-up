#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) 1) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<size_t L>
class SwapToEqualizeSum {
public:

    pair<uint32_t, uint32_t> solve(array<uint32_t, L> &first,
                                   array<uint32_t, L> &second) {

        // O(N * log(N))
        sort(first.begin(), first.end());
        // O(N * log(N))
        sort(second.begin(), second.end());
        // O(N)
        uint32_t first_sum = accumulate(first.begin(), first.end(), 0u);
        // O(N)
        uint32_t second_sum = accumulate(second.begin(), second.end(), 0u);
        uint32_t final_sum = (first_sum + second_sum) / 2;
        cout << "final sum: " << final_sum << " diff: "
             << (final_sum > first_sum ? final_sum - first_sum : first_sum - final_sum)
             << endl;
        int i = 0;
        int j = L - 1;
        auto small_distance = UINT32_MAX;
        while (i < L && j >= 0) {
            auto second_temp_sum = second_sum + first[i];
            auto first_temp_sum = first_sum - first[i];
            while (first_temp_sum + second[j] > second_temp_sum - second[j]) {
                auto current_diff = first_temp_sum + second[j] - final_sum;
                small_distance = small_distance < current_diff ? small_distance : current_diff;
                j--;
            }
            if (first_temp_sum + second[j] == second_temp_sum - second[j]) {
                return {i, j};
            }
            i++;
        }
        cout << "small_distance : " << small_distance << endl;
        return {UINT32_MAX, UINT32_MAX};
    }
};


template<typename T, size_t L>
ostream &operator<<(ostream &out, array<T, L> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;

}

template<size_t L>
class FastSwapToEqualizeSum {
public:

    pair<uint32_t, uint32_t> solve(array<uint32_t, L> &first,
                                   array<uint32_t, L> &second) {

        pair<uint32_t, uint32_t> solution{UINT32_MAX, UINT32_MAX};
        // O(N)
        uint32_t first_sum = accumulate(first.begin(), first.end(), 0u);
        // O(N)
        uint32_t second_sum = 0u;
        unordered_map<uint32_t, uint32_t> positions;
        for (auto i = 0u; i < second.size(); i++) {
            uint32_t value = second[i];
            positions[value] = i;
            second_sum += value;
        }
        uint32_t final_sum = (first_sum + second_sum) / 2;
        auto diff = (final_sum > first_sum ? final_sum - first_sum : first_sum - final_sum);
        cout << "first_sum: " << first_sum
             << " second_sum: " << second_sum
             << " final sum: " << final_sum
             << " diff: " << diff
             << endl;

        // Trying O(N)
        for (auto index = 0u; index < first.size(); index++) {
            auto value = first[index];
            auto partial_second = second_sum + value;

            if (partial_second >= final_sum) {
                uint32_t searched = partial_second - final_sum;
                bool exist = positions.count(searched) > 0;
                if (exist) {
                    solution.first = index;
                    solution.second = positions[searched];
                    break;
                }
            }
        }
        return solution;
    }
};

ostream &operator<<(ostream &out, const pair<uint32_t, uint32_t> &subject) {
    out << '(' << subject.first << ',' << subject.second << ')';
    return out;
}

template<size_t L>
void check(array<uint32_t, L> first,
           array<uint32_t, L> second,
           const pair<uint32_t, uint32_t> &expected) {
    FastSwapToEqualizeSum<L> solver;
    auto actual = solver.solve(first, second);

    cout << "- " << first << endl;
    cout << "- " << second << endl;
    bool equal = actual == expected;
    cout << (equal ? '=' : '!') << ' ' << actual << ' ' << expected << endl;
}

int main(int argc, char **argv) {
    const uint32_t ARRAY_LENGTH = 128;
    Dice dice;
    array<uint32_t, ARRAY_LENGTH> first_array{0};
    array<uint32_t, ARRAY_LENGTH> second_array{0};
    for (auto i = 0u; i < ARRAY_LENGTH; ++i) {
        first_array[i] = dice.next(0, 10 * ARRAY_LENGTH);
        second_array[i] = dice.next(0, 10 * ARRAY_LENGTH);
    }
    check<3>({0, 1, 3}, {1, 2, 3}, {0, 0});
    check<3>({1, 2, 3}, {0, 1, 3}, {0, 0});
     check<4>({4, 1, 2, 3}, {4, 0, 1, 3}, {0, 3});
    check<4>({4, 0, 1, 3}, {4, 1, 2, 3}, {1, 1});
    check<5>({0, 1, 3, 4, 9}, {0, 1, 2, 3, 5}, {2, 0});

    check<ARRAY_LENGTH>(first_array, second_array, {0, 0});
}