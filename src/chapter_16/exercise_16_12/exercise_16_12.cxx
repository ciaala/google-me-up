#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint64_t next(uint64_t base, uint64_t maximum) {
        std::uniform_int_distribution<uint64_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class XMLEncoding {
private:
    const string &xml;
    const unordered_map<string, int> &elements;
    const unordered_map<string, int> &attributes;
    const char TOKEN_MINOR = '<';
    const char TOKEN_MAJOR = '>';
    const char TOKEN_END = '/';
    const char TOKEN_QUOTE = '"';
    const char TOKEN_SINGLEQUOTE = '\'';
    const char TOKEN_ASSIGNMENT = '=';
    const char TOKEN_ESCAPE_QUOTE = '\\';

    bool matchText(stringstream &result, uint64_t &index) {
        uint64_t found = xml.find(TOKEN_MINOR, index + 1);
        if (found != string::npos) {
            result << " \'" << xml.substr(index, (found) - index) << '\'';
            index = found;
            return true;
        }
        return false;
    }

    bool matchAttribute(stringstream &result, uint64_t &index) {
        for (auto &attribute: attributes) {
            auto &attr = attribute.first;
            auto found = xml.compare(index, attr.length(), attr);
            if (found == 0) {
                result << ' ' << attribute.second;
                index += attr.length();
                if (xml[index] == TOKEN_ASSIGNMENT
                    && (xml[index + 1] == TOKEN_QUOTE || xml[index + 1] == TOKEN_SINGLEQUOTE)) {
                    auto currentQuote = xml[index + 1];
                    auto next = xml.find(currentQuote, index + 2);
                    while (next != string::npos && xml[next - 1] == TOKEN_ESCAPE_QUOTE) {
                        next = xml.find(currentQuote, next + 1);
                    }
                    if (next != string::npos) {
                        result << ' ' << xml.substr(index + 1, next - index);
                        index = next + 1;
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    bool matchWhiteSpace(uint64_t &index) {
        auto found = xml.find_first_not_of(' ', index);
        if (found != string::npos) {
            index = found;
        }
        return false;
    }

    bool matchElement(stringstream &result, uint64_t &index) {
        for (auto &element: elements) {
            auto elm = element.first;
            auto elmLength = elm.length();
            auto found = xml.compare(index, elmLength, elm);
            if (found == 0) {
                result << ' ' << element.second;
                index += elmLength;
                while (xml[index] != TOKEN_MAJOR) {
                    matchWhiteSpace(index);
                    if (!matchAttribute(result, index)) {
                        return false;
                    }
                }
                index += 1;
                auto matches = true;
                while (matches) {
                    matches = false;
                    if (xml[index] != TOKEN_MINOR) {
                        matches = matchText(result, index);
                    } else if (xml[index] == TOKEN_MINOR) {

                        auto tmpIndex = index +1;
                        matches = matchElement(result, tmpIndex);
                        if (matches) {
                            index = tmpIndex;
                        }
                    }
                }

                if (xml[index] == TOKEN_MINOR
                    && xml[index + 1] == TOKEN_END
                    && xml.compare(index + 2, elmLength, elm) == 0
                    && xml[index + 2 + elmLength] == TOKEN_MAJOR) {
                    result << " 0";
                    index += 3 + elmLength;
                    return true;
                }
            }
        }
        return false;
    };

public:
    XMLEncoding(string &xml,
                const unordered_map<string, int> &elements,
                const unordered_map<string, int> &attributes)
            : xml(xml), elements(elements), attributes(attributes) {
    }

    string encode() {
        stringstream result;
        uint64_t index = 0;
        while (index != xml.length()) {
            matchWhiteSpace(index);
            if (xml[index] == TOKEN_MINOR) {
                index += 1;
                if (!matchElement(result, index)) {
                    break;
                }
            }
        }
        return result.str();
    }
};


int
main(int argc, char **argv) {
    string xml = "<family lastName='McDowel' state='CA'>"
                 "  <person firstName='Gayle'>Some Message</person>"
                 "</family>";
    unordered_map<string, int> elements = {
            {"family", 1},
            {"person", 2},
    };
    unordered_map<string, int> attributes = {
            {"firstName", 3},
            {"lastName",  4},
            {"state",     5}
    };
    XMLEncoding xmlEncoding(xml, elements, attributes);
    cout << xml << endl;
    cout << xmlEncoding.encode() << endl;
    return 0;
}