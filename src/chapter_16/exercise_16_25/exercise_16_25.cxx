#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <list>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename K, typename V>

class CacheLRU { ;
    unordered_map<K, pair<V, typename list<K>::iterator> > cache;
    list<K> keyList;
    size_t maximum_size;
public:

    CacheLRU(size_t maximum_size = 25) : maximum_size(maximum_size) {

    }

    V get(K key) {
        if (cache.count(key) > 0) {
            auto & value_compo = cache[key];
            keyList.erase(value_compo.second);
            value_compo.second = keyList.insert(keyList.begin(), key);
        }
        return cache[key].first;
    }

    void insert(K key, V value) {
        if (cache.size() == maximum_size) {
            K last = keyList.back();
            cache.erase(last);
            keyList.pop_back();
        }
        typename list<K>::iterator it = keyList.insert(keyList.begin(), key);
        pair<V, typename list<K>::iterator> p = {value, it};
        cache[key] = p;
    }

    void print() {
        for (K key : this->keyList) {
            cout << key << ' ';
        }
        cout << endl;

        for (auto it = cache.begin(); it != cache.end(); it++) {
            cout << (*it).first << ' ';
        }
        cout << endl;
    }

};

template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T>
ostream &operator<<(ostream &out, const pair<T, T> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

int main(int argc, char **argv) {
    CacheLRU<int, string> lru;
    for (int i = 0u; i < 30; i++) {
        lru.insert(i, "ciao_" + to_string(i));
    }
    lru.get(5);
    lru.insert(31, "bastardo");
    lru.print();
}