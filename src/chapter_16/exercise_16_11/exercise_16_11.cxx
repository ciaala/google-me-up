#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class DivingBoard {
public:
    void solve(uint32_t k, uint32_t shortLength, uint32_t longLength) {
        //unordered_set<uint32_t> seenSums;
        int space = (int) log10(k) + 1;
        for (int i = 0; i <= k; i++) {
            auto a = i;
            auto b = k - i;
            auto sum = a * shortLength + b * longLength;

            cout << setfill(' ') << setw(space) << a << setw(space + 1) <<
                 b << setw(space + 2) << sum << endl;

        }
        cout << endl;

    };
};

int main(int argc, char **argv) {
    DivingBoard divingBoard;
    divingBoard.solve(1, 1, 2);
    divingBoard.solve(2, 1, 2);
    divingBoard.solve(3, 1, 2);
    divingBoard.solve(4, 1, 2);

    divingBoard.solve(16, 2, 4);

    divingBoard.solve(10, 1, 2);
    divingBoard.solve(10, 1, 3);
    divingBoard.solve(10, 3, 11);
}