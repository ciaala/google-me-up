#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

enum Color {
    N, Y, R, G, B,
};

class MasterMind {
public:
    pair<int, int> guess(array<Color, 4> solution, array<Color, 4> guess) {
        // unordered_set<Color> guessRemainings;
        int _hits = 0;
        int _guess = 0;
        for (int i = 0; i < solution.size(); i++) {
            if (solution[i] == guess[i]) {
                solution[i] = N;
                guess[i] = N;
                _hits++;
            }
        }
        if (_hits < solution.size()) {
            for (int i = 0; i < solution.size(); i++) {
                if (solution[i] != N) {
                    for (int j = 0; j < guess.size(); j++) {
                        if (solution[i] == guess[j]) {
                            guess[j] = N;
                            _guess++;
                            break;
                        }
                    }
                }
            }
        }
        return {_hits, _guess};
    }
};


ostream &operator<<(ostream &out, const array<Color, 4> &colors) {
    out << '[' << colors[0] << ' ' << colors[1] << ' ' << colors[2] << ' ' << colors[3] << ']';
    return out;
}

ostream &operator<<(ostream &out, const pair<int, int> &hits_guess) {
    out << hits_guess.first << ',' << hits_guess.second;
    return out;
}

void
check(const array<Color, 4> &solution, const array<Color, 4> &guess, const pair<int, int> &expected) {
    MasterMind mm;
    auto actual = mm.guess(solution, guess);
    auto equal = actual.first == expected.first && actual.second == expected.second;
    cout << solution << ' ' << guess << " => " << actual << " == " << expected << (equal ? '!' : '?') << endl;
}

int main(int argc, char **argv) {
    check({{Y, R, G, B}}, {{Y, Y, Y, Y}}, {1, 0});
    check({{Y, R, G, B}}, {{Y, Y, R, R}}, {1, 1});
    check({{Y, R, G, B}}, {{Y, R, Y, Y}}, {2, 0});
    check({{Y, R, G, B}}, {{R, G, B, Y}}, {0, 4});
    check({{Y, R, G, B}}, {{Y, R, G, B}}, {4, 0});

}