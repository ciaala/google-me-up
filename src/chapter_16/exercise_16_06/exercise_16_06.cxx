#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>

using namespace std;
template <typename NUMBER>
class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    NUMBER next(NUMBER base, NUMBER maximum) {
        std::uniform_int_distribution<NUMBER> distribution(base, maximum);
        return distribution(generator);
    }
};

vector<int> makeVector(uint32_t size) {
    vector<int> result(size);
    result.reserve(size);
    Dice<int> dice;
    for (auto i = 0u; i < size; i++) {
        result[i] = dice.next(INT32_MIN, INT32_MAX);
    }
    return result;
}

class SmallestDifference {
private:

public:
    uint32_t solve(vector<int> &&first, vector<int> &&second) {

        pair<int, int> winningPair;
        winningPair.first = UINT32_MAX, winningPair.second = UINT32_MAX;

        sort(first.begin(), first.end());
        sort(second.begin(), second.end());
        auto firstRunner = 0u, secondRunner = 0u;
        uint32_t result = UINT32_MAX;
        while (firstRunner < first.size() && secondRunner < second.size() && result != 0) {
            int diff = first[firstRunner] - second[secondRunner];
            if (diff >= 0 && diff < result) {
                result = (uint32_t) diff;
                winningPair.first = first[firstRunner], winningPair.second = second[secondRunner];
            } else if (diff < 0 && firstRunner < first.size()) {
                firstRunner++;
            } else if (diff > 0 && secondRunner < second.size()) {
                secondRunner++;
            } else {
                cout << "ERROR" << endl;
            }
        }
        if (result < UINT32_MAX) {
            cout << "Winning couple: " << winningPair.first << " - " << winningPair.second << " => "
                 << result << endl;
        }
        return result;
    }
};

int main(int argc, char **argv) {
    SmallestDifference difference;
    difference.solve({1, 2, 3}, {4, 5, 6});
    difference.solve({1, 2, 3}, {3, 5, 6});
    difference.solve({1, 2, 3, 4, 5, 6, 7}, {0, 0, 7});
    difference.solve(makeVector(33), makeVector(33));
    return 0;
}