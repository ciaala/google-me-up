#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class Sequence {
public:
    int start;
    int end;
    int sum;
};

class ContigousSequence {

public:
    Sequence solve(const vector<int> data) {

        Sequence solution;
        solution.sum = INT32_MIN;
        solution.start = INT32_MIN;
        solution.end = INT32_MIN;
        if (!data.empty()) {
            Sequence current{.sum =0, .start = INT32_MIN, .end = INT32_MIN};
            Sequence positive{.sum =0, .start = INT32_MIN, .end = INT32_MIN};
            /*    current.sum = 0;
                current.start = INT32_MIN;
                current.end = INT32_MIN;*/
            for (auto i = 0u; i < data.size(); ++i) {
                //if (positive)
                if (current.sum + data[i] > 0) {
                    current.end = i;
                    current.sum += data[i];
                    if (current.start == INT32_MIN) {
                        current.start = i;
                    }
                    if (current.sum > solution.sum) {
                        solution = current;
                    }
                } else {
                    // current.
                    current.sum = 0;
                    current.start = INT32_MIN;
                    current.end = INT32_MIN;
                }
            }
        }
        return solution;
    }
};

ostream &operator<<(ostream &out, const vector<int> &data) {
    out << '[';
    for (auto const &value : data) {
        out << ' ' << value;
    }
    out << " ]";
    return out;
}

ostream &operator<<(ostream &out, const Sequence &sequence) {
    out << sequence.sum << " [" << sequence.start << ' ' << sequence.end << ']';
    return out;
}

int main(int argc, char **argv) {
    vector<int> data;
    data.reserve(200);
    Dice dice;

    for (auto i = 0; i < 200; ++i) {
        data.emplace_back(dice.next(0, 200) - 100);
    }
    ContigousSequence solver;
    cout << data << endl;
    cout << solver.solve(data) << endl;
    cout << solver.solve({1, 2, 3, 4}) << endl;
    cout << solver.solve({1, 2, 3, -4}) << endl;

    cout << solver.solve({1, 2, -3, 3}) << endl;
    cout << solver.solve({1, -1, -3, 3}) << endl;
    cout << solver.solve({2, 1, -1, -3, 3}) << endl;

    cout << solver.solve({0, -1, 1, -1, -3, 1}) << endl;
    cout << solver.solve({0}) << endl;
    cout << solver.solve({}) << endl;
}