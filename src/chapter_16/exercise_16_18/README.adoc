= 16.18 Pattern Matching

You are given two strings, `pattern` and `value`.
The pattern string consists of just the letters `a` and `b`, describing the pattern withing a string.
For example, the string `catcatgocatgo` matches the pattern `aabab` (where cat is `a` and go is `b`).
It also matches patterns like `a`,`ab`, and `b` . Write a method to determine if value matches pattern.


== Analysis

We should map the the pattern to a combination of characters.

1. We can calculate the number of characters in each group.
We can write an equation that says Sum of Group.length X number of repetitions = total.length.
It does not solve the problem, but it allows to reduce the combination to check.

2. We need to simulate the solution.
a. We assume length 1 to n for the first group to match. We can bound the upper limit of that length by knowing the repetition.
Simulate from 1 to `value.length / group[0].repetition`.
b. We proceed to match the remaining. `group[x].length = value.length - group[0].length * group[0].repetition`
c. We proceed to verify the match substituting according to the `pattern sequence` the groups in the `value`.
d. We succeed if we match everything to the end of the pattern.
e. Nothing is left in value. Given by the math checks


