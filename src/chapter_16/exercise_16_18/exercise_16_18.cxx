#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class PatternMatcher {
    array<char, 2> groupId{'a', 'b'};

    array<size_t, 2> groupRepetition{0, 0};
    array<size_t, 2> groupMaximumLength{0, 0};
public:
    bool matches(const string &value, const string &pattern) {

        int firstIndex = -1;
        for (char c : pattern) {
            int index = c == groupId[0] ? 0 : 1;
            groupRepetition[index] += 1;
        }
        groupMaximumLength[0] = (value.length() - groupRepetition[1]) / groupRepetition[0];
        groupMaximumLength[1] = (value.length() - groupRepetition[0]) / groupRepetition[1];
        for (size_t i = 1; i < groupMaximumLength[0]; i++) {
            auto secondGroupLength = (value.length() - i * groupRepetition[0]) / groupRepetition[1];
            auto comboLength = secondGroupLength * groupRepetition[1] + i * groupRepetition[0];
            array<size_t, 2> lengths{i, secondGroupLength};
            if (comboLength == value.length()) {
                //   cout << " + " << i << ',' << secondGroupLength << '=' << comboLength << endl;
                size_t valueIndex = 0;
                array<string, 2> groupString{"", ""};
                for (size_t k = 0; k < pattern.length(); ++k) {
                    char c = pattern[k];
                    int index = c == groupId[0] ? 0 : 1;

                    if (groupString[index].empty()) {
                        groupString[index] = value.substr(valueIndex, lengths[index]);
                        valueIndex += lengths[index];
                    } else {
                        if (value.compare(valueIndex, lengths[index], groupString[index]) != 0) {
                            cout << " - " << pattern.substr(0, k) << '_' << c << '_' << pattern.substr(k + 1);
                            cout << ": " << value.substr(valueIndex, lengths[index]) << "!=" << groupString[index]
                                 << endl;
                            break;
                        }
                        valueIndex += lengths[index];
                    }
                }
                if (valueIndex == value.length()) {
                    cout << '!' << groupString[0] << ',' << groupString[1] << endl;
                }
            } else {
                cout << " - " << i << ',' << secondGroupLength << '=' << comboLength << endl;
            }
        }
        return true;
    }
};

int main(int argc, char **argv) {
    PatternMatcher matcher;
    cout << "BelloBelloIlMondo" << ',' << "aab" << endl;
    matcher.matches("BelloBelloIlMondo", "aab");
}