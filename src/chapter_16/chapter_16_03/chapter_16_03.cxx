
#include <iostream>
#include <array>
#include <cmath>

using namespace std;

class Point2D {
public:
    Point2D(double x, double y) : x(x), y(y) {}

    double x;
    double y;
};

class Segment {
public:
    Segment(double x1, double y1, double x2, double y2) : first(x1, y1), second(x2, y2) {

    }
    Point2D first;
    Point2D second;
};

ostream &operator<<(ostream &out, Point2D &point) {
    out << '(' << point.x << ',' << point.y << ')' << endl;
    return out;
}

class FindIntersection {
private:
    void findLine(const Segment &segment, double &m, double &q) {
        if (segment.second.y != segment.first.y) {
            m = (segment.second.y - segment.first.y) / (segment.second.x - segment.first.x);
            q = segment.first.y - m * segment.first.x;
        } else {
            m = INFINITY;
            q = segment.first.x;
        }
    }

    bool verify(const Segment &s1, const Point2D &intersection) {
        return (((s1.first.x <= intersection.x) && (s1.second.x >= intersection.x))
                || ((s1.first.x >= intersection.x) && (s1.second.x <= intersection.x)))
               && (((s1.first.y <= intersection.y) && (s1.second.y >= intersection.y)) ||
                   ((s1.first.y >= intersection.y) && (s1.second.y <= intersection.y)));
    }

public:
    bool intersection(const Segment &s1, const Segment &s2,
                      Point2D &intersection) {
        double m1, q1;
        findLine(s1, m1, q1);
        double m2, q2;
        findLine(s2, m2, q2);
        if (m1 == INFINITY && m2 == INFINITY) {
        }
        if (m1 == INFINITY ^ m2 == INFINITY) {
            auto dir = m1 == INFINITY ? m2 : m1;
            auto vx = m1 == INFINITY ? q1 : q2;
            auto q = m1 == INFINITY ? q2 : q1;
            intersection.x = vx;
            intersection.y = dir * vx + q;
            return verify(s1, intersection) && verify(s2, intersection);
        } else if (m1 != m2) {
            intersection.x = (q2 - q1) / (m1 - m2);
            intersection.y = m1 * intersection.x + q1;
            return verify(s1, intersection) && verify(s2, intersection);
        } else if (q1 == q2) {
            cout << "segments are one over the other" << endl;
            return false;
        } else {
            return false;
        }
    }
};


void lookForIntersection(const Segment &s2) {
    FindIntersection alg;
    Point2D intersection(0, 0);
    auto s1 = Segment(1, 1, 20, 20);
    auto isValid = alg.intersection(s1, s2, intersection);
    if (isValid) {
        cout << "intersection: " << intersection << endl;
    } else {
        cout << "no intersection" << endl;
    }
}
int main(int argc, char **argv) {
    lookForIntersection(Segment(10, 0, 14, 9));
    lookForIntersection(Segment(10,0, 9, 14));

}
