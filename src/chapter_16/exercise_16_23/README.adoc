= 16.23 Rand7 from Rand5

Implement a method rand7() given rand5(). That is, given a method that generates a random number between 0 and 4 (inclusive), write a method that generates a random number between 0 and 6 (inclusive);


= Analysis

My approach is brute force in some way.
```
rand7 = (rand5 + rand5 + rand5 + rand5 + rand5 + rand5 + rand5 + rand5 + rand5 ) % 7;
```
A computational cheaper approach could be to generate up to 24 and then cut it down by `module 7`

```
rand7 = { value = (5*rand5 + rand5 ) while ( value > 20 ) { value = (5*rand5 + rand5) } ; return value;
```