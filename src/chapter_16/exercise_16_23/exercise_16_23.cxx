#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename T>
ostream &operator<<(ostream &out, vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

Dice dice;

uint32_t rand7() {
    return (dice.next(0, 4) + dice.next(0, 4) + dice.next(0, 4)
            + dice.next(0, 4) + dice.next(0, 4) + dice.next(0, 4)
            + dice.next(0, 4))
           % 7;
}

uint32_t other_rand7() {
    auto value = 5 * dice.next(0, 4) + dice.next(0, 4);
    while (value > 20) {
        value = 5 * dice.next(0, 4) + dice.next(0, 4);
    }
    return value % 7;
}

int main(int argc, char **argv) {
    const int SIZE = 7;
    uint32_t count_them_all[SIZE] = {0};
    for (auto i = 0u; i < 500000; i++) {
        auto value = other_rand7();
        count_them_all[value] += 1;
    }

    for (auto i = 0u; i < SIZE; i++) {
        cout << setw(3) << i << ' ' << count_them_all[i] << endl;
    }
    cout << endl;
}