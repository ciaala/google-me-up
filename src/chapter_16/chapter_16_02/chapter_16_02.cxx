//
// Created by Francesco Fiduccia on 2019-01-20.
//
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <unordered_map>

using namespace std;


class TextDictionary {
    string text;
    unordered_map<string, uint32_t> wordCount;
public:
    TextDictionary(string &text) : text(text) {
        uint64_t index = 0;
        while (index < text.length()) {
            unsigned long nextIndex = text.find_first_of(" \n.,';:?!", index);
            if (nextIndex == string::npos) {
                // cout << "Last index :" << index << endl;
                return;
            }

            string word = text.substr(index, nextIndex - index);
            if (word.length() > 0) {
                if (wordCount.count(word) > 0) {
                    wordCount[word] += 1;
                    // cout << "+" << word << endl;
                } else {
                    wordCount[word] = 1;
                    // cout << "@" << word << endl;
                }
            }
            index = nextIndex + 1;

        }
        cout << "Total words: " << wordCount.size() << endl;

    }

    void countWords(string word) {
        cout << "Word '" << word << "' has been found: " << wordCount[word] << endl;
    }

    void printStats() {
        vector<pair<string, uint32_t>> matches_List(wordCount.begin(), wordCount.end());
        sort(matches_List.begin(), matches_List.end(), [](auto &e1, auto &e2) { return e1.second < e2.second; });
        for (auto &entry : matches_List) {
            cout << entry.first << ";" << entry.second << endl;
        }
    }
};

class TextFileReader {


public:
    static string read(string &filename) {
        string line;
        ifstream my_file(filename);
        stringstream buffer;
        if (my_file.is_open()) {
            while (getline(my_file, line)) {
                buffer << line << '\n';
            }
            my_file.close();
        } else {
            cout << "File '" << filename << "' could not be read." << endl;
        }
        return buffer.str();
    }
};

int main(int argc, char **argv) {
    string filename = "/Users/crypt/projects/google-me-up/resource/Shakespear's Sonnets.txt";
    string content = TextFileReader::read(filename);
    transform(content.begin(), content.end(), content.begin(), ::tolower);
    TextDictionary td(content);
/*
    td.countWords("thee");
    td.countWords(" ");
    td.countWords("\n");
    td.countWords("love");
    */
    td.printStats();
    return 0;
}