#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

using namespace std;

class CountTrailingZeroInFactorial {
    unordered_map<uint32_t, uint32_t> memo;
public:
    uint32_t solve(uint32_t n) {

        // all the numbers between 1 to n are part of the factorial
        auto result = 0u;
        for (uint32_t i = 5u; i <= n; i += 5) {
            auto r = i / 5;
            if (memo.count(r) > 0) {
                r = 1 + memo[r];
            } else {
                r = 1;
            }
            memo[i] = r;

            // to get the result.
            // we need to sum them all
            result += r;
        }
        return result;
    }
};

int main(int argc, char **argv) {
    CountTrailingZeroInFactorial solver;
    vector<pair<uint32_t, uint32_t>> test_example =
            {{0, 0}, {4, 0}, {5, 1}, {6, 1}, {11, 2}, {14, 2}, {16, 3}, {21, 4}, { 24, 4 }, { 26, 6 }};
    for ( auto &p : test_example) {
        auto value = solver.solve(p.first);
        cout << p.first << "->" << value << ": " << p.second << (value != p.second ? '?': '!') << endl;
    }
}