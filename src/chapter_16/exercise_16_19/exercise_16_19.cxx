#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

using cell = pair<uint32_t, uint32_t>;

class Pond {
public:
    vector<cell> cells;
    uint32_t size = 0;
    uint32_t mergedTo = 0;
};

class PondSize {
private:

    uint32_t
    getRootPond(const vector<Pond> &ponds, const uint32_t pondIndex) {
        auto rootIndex = pondIndex;
        while (ponds[rootIndex].mergedTo != UINT32_MAX) {
            rootIndex = ponds[rootIndex].mergedTo;
        }
        return rootIndex;
    }

    void
    mergePond(vector<Pond> &ponds, const uint32_t masterPond, const uint32_t subPond) {
        auto merged = getRootPond(ponds, subPond);
        auto master = getRootPond(ponds, masterPond);
        if (merged != master) {
            ponds[merged].mergedTo = master;
            ponds[master].size += ponds[merged].size;
            cout << "merging : " << merged << " into " << master << endl;
        }
    }

    uint32_t
    addToPond(vector<Pond> &ponds,
              array<array<uint32_t, 30>, 30> &exploration,
              uint32_t i,
              uint32_t j,
              uint32_t otherPond) {
        uint32_t pondIndex = getRootPond(ponds, otherPond);
        exploration[i][j] = pondIndex;
        Pond &pond = ponds[pondIndex];
        pond.cells.emplace_back(i, j);
        pond.size += 1;
        return pondIndex;
    }

public:
    vector<Pond> calculate(const array<array<uint32_t, 30>, 30> &land) {
        array<uint32_t, 30> base;
        base.fill(UINT32_MAX);
        array<array<uint32_t, 30>, 30> exploration;
        exploration.fill(base);
        vector<Pond> ponds;
        for (auto i = 0u; i < 30; ++i) {
            for (auto j = 0u; j < 30; ++j) {
                if (land[i][j] == 0) {
                    auto pondIndex = UINT32_MAX;
                    if (i > 0 && exploration[i - 1][j] != UINT32_MAX) {
                        pondIndex = addToPond(ponds, exploration, i, j, exploration[i - 1][j]);
                    }
                    if (j > 0 && exploration[i][j - 1] != UINT32_MAX) {
                        if (pondIndex != UINT32_MAX) {
                            mergePond(ponds, pondIndex, exploration[i][j - 1]);
                        } else {
                            pondIndex = addToPond(ponds, exploration, i, j, exploration[i][j - 1]);
                        }
                    }
                    if (i > 0 && j > 0 && exploration[i - 1][j - 1] != UINT32_MAX) {
                        if (pondIndex != UINT32_MAX) {
                            mergePond(ponds, pondIndex,
                                      exploration[i - 1][j - 1]);
                        } else {
                            pondIndex = addToPond(ponds, exploration, i, j, exploration[i - 1][j - 1]);
                        }
                    }
                    if (pondIndex == UINT32_MAX) {
                        pondIndex = (uint32_t) ponds.size();
                        ponds.emplace_back(Pond());
                        ponds[pondIndex].size = 1;
                        ponds[pondIndex].mergedTo = UINT32_MAX;
                        ponds[pondIndex].cells.emplace_back(i, j);
                        exploration[i][j] = pondIndex;
                    }
                }
            }
        }
        return ponds;
    }

};

ostream &operator<<(ostream &out, const vector<cell> &cells) {
    out << '[';
    for (const auto &cell : cells) {
        out << " (" << setw(2) << cell.first << ',' << setw(2) << cell.second << ')';
    }
    out << " ]";
    return out;
}

int main(int argc, char **argv) {
    Dice dice;
    array<array<uint32_t, 30>, 30> land{0};
    for (auto i = 0; i < 30; ++i) {
        for (auto j = 0; j < 30; ++j) {
            land[i][j] = dice.next(0, 3);
        }
    }
    PondSize pondinator;
    auto ponds = pondinator.calculate(land);
    for (auto i = 0u; i < ponds.size(); ++i) {
        auto &pond = ponds[i];
        cout << setw(3) << i;
        if (pond.mergedTo != UINT32_MAX) {
            cout << " M ";
        } else {
            cout << "   ";
        }
        cout << setw(2) << pond.size << ' ' <<pond.cells << endl;
    }
}