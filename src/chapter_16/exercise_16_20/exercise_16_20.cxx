#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <fstream>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class T9Dictionary {
private:

    unordered_multimap<string, string> t9Words;

    string wordToT9(string word) {
        stringstream result;

        for (auto c : word) {
            c = tolower(c);
            switch (c) {
                case 'a':
                case 'b':
                case 'c': {
                    result << '2';
                    break;
                }
                case 'd':
                case 'e':
                case 'f': {
                    result << '3';
                    break;
                }
                case 'g':
                case 'h':
                case 'i': {
                    result << '4';
                    break;
                }
                case 'j':
                case 'k':
                case 'l': {
                    result << '5';
                    break;
                }
                case 'm':
                case 'n':
                case 'o': {
                    result << '6';
                    break;
                }
                case 'p':
                case 'q':
                case 'r':
                case 's': {
                    result << '7';
                    break;
                }
                case 't':
                case 'u':
                case 'v': {
                    result << '8';
                    break;
                }
                case 'w':
                case 'x':
                case 'y':
                case 'z': {
                    result << '9';
                    break;
                }
                default: {
                    cout << "What is it '" << c << "' ? " << word << endl;
                }

            }
        }
        return result.str();
    }

public :
    void addWord(string word) {
        string t9 = wordToT9(word);
        this->t9Words.emplace(t9, word);
    }

    vector<string *> findT9SimilarWord(const string &number) {
        vector<string *> result;
        if (t9Words.count(number) > 0) {
            auto range = t9Words.equal_range(number);
            for_each(range.first,
                     range.second,
                     [&result](auto &word) {
                         string *pointer = &(word.second);
                         result.emplace_back(pointer);
                     });
            return result;
        }
        return result;
    }
};

void readDictionary(T9Dictionary &dictionary, string &filename) {

    ifstream input(filename);
    while (!input.eof()) {
        string line;
        getline(input, line);
        dictionary.addWord(line);
    }
}

ostream &operator<<(ostream &out, vector<string *> &result) {
    for_each(result.begin(), result.end(), [&out](auto *word) { out << *word << ' '; });
    return out;

}

int main(int argc, char **argv) {
    string filename = "/Users/crypt/projects/google-me-up/resource/voa-special-english-word-list.txt";
    T9Dictionary dictionary;
    readDictionary(dictionary, filename);

    string value = "223";
    cout << value << ": " << dictionary.findT9SimilarWord(value) << endl;

}