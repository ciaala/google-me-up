#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class MaximumOfTwoNumber {
    int flip(int bit) {
        return bit ^ 1;
    }

    int sign(int number) {
        return flip((number >> 31) & 0x1);
    }

public:

    int solve(int first, int second) {
        int sign_first = sign(first);
        int sign_second = sign(second);
        int diff = first - second;
        int sign_diff = sign(diff);

        int use_sign_of_first = sign_first ^sign_second;

        int use_sign_of_diff = flip(use_sign_of_first);

        int k = use_sign_of_first * sign_first + use_sign_of_diff * sign_diff;
        int q = flip(k);

        return first * k + second * q;
    }
};

void check(int first, int second) {
    MaximumOfTwoNumber maximum;
    auto expected = max(first, second);
    auto value = maximum.solve(first, second);
    cout << first << ", " << second << " => " << value << (value == expected ? '!' : '?') << endl;
}

int main(int argc, char **argv) {
    check(1, 2);
    check(2, 1);
    check(-1, -2);
    check(0, 1);
    check(1, 0);
    check(-1, 0);
    check(0, -1);
    check(INT32_MIN, INT32_MIN + 1);
    check(INT32_MIN, INT32_MIN + 1);
    check(INT32_MAX, INT32_MIN);
    check(INT32_MIN, INT32_MAX);
}