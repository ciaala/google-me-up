#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <unistd.h>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class FindAlreadySortedIndexes {
public:
/*
    int bisect(int start, int end, int value, const vector<int> &vector) {

        if (start >= end) {
            return start;
        }

        auto j = (start + end) / 2;
//        if (value > vector[j - 1]) {
//            return j;
//        }
        if (value < vector[j]) {
            return bisect(start, j - 1, value, vector);
        } else {
            return bisect(j + 1, end, value, vector);
        }
    }
*/
    int bisect(int start, int end, int value, const vector<int> &vector) {
        if (start >= end) {
            return start;
        }
        int diff = (end - start);
        if (diff == 1) {
            if  (vector[start] < value && value < vector[end]) {
                return end;
            } else {
                return start;
            }
        }
        int j = start + (diff / 2);
        if (value < vector[j]) {
            auto anEnd = j ;
            return bisect(start, anEnd, value, vector);
        } else {
            auto anStart = j;
            return bisect(anStart, end, value, vector);
        }
    }

    pair<unsigned, unsigned> solve(const vector<int> &sequence) {
        if (sequence.empty()) {
            return {INT_MIN, INT_MAX};
        }
        if (sequence.size() == 1) {
            return {0, 0};
        }
        int m = INT_MIN, n = INT_MAX;
        int max = INT_MIN;
        for (int i = 0; i < sequence.size(); i++) {
            auto value = sequence[i];
            if (max < value) {
                // the current element is in order against the previous max
                // after ordering it will be in order too
                max = value;
            } else {
                auto end = m != INT_MIN ? m : i - 1;
                auto mNew = bisect(0, end, value, sequence);
                m = end < mNew ? end : mNew;
                n = i;
                // the current element is lower then the maximum value soon.
                // cell up to
            }
        }
        if (m == INT_MIN) {
            m = n = sequence.size() - 1;
        }
        return {m, n};
    }
};

ostream &operator<<(ostream &out, const pair<int, int> &_pair) {
    out << _pair.first << ',' << _pair.second;
    return out;
}

ostream &operator<<(ostream &out, const vector<int> &sequence) {
    out << "[ ";
    for (int i : sequence) {
        out << i << ' ';
    }
    out << ']';
    return out;
}

bool operator==(const pair<int, int> &alpha, const pair<int, int> &beta) {
    return alpha.first == beta.first && alpha.second == beta.second;
}

void check(const vector<int> &sequence, const pair<int, int> &expected) {
    FindAlreadySortedIndexes indexer;
    auto actual = indexer.solve(sequence);
    //cout << sequence << flush;
    //  sleep(1);
    cout << (expected == actual ? '!' : '?') << ' ' << sequence << " -> " << actual << " : " << expected
         << endl;
}

int main(int argc, char **argv) {
    check(vector<int>(), {INT_MIN, INT_MAX});
    check({0}, {0, 0});
    check({0, 1}, {1, 1});
    check({0, 1, 2}, {2, 2});
    check({0, 1, 2, 3}, {3, 3});
    check({0, 3, 6, 9, 12, 13, 15}, {6, 6});
    check({1, 0}, {0, 1});
    check({1, 0, 2}, {0, 1});
    check({1, 0, 2, 3}, {0, 1});
    check({0, 2, 1, 3}, {1, 2});
    check({0, 2, 3, 1}, {1, 3});
    check({0, 3, 9, 6, 12, 13, 15}, {2, 3});
    check({0, 3, 6, 12, 9, 13, 15}, {3, 4});
    check({0, 3, 6, 12, 13, 9, 15}, {3, 5});
    check({0, 3, 6, 12, 13, 9, 15, 1}, {1, 7});
    check({15, 3, 6, 12, 13, 9, 14, 1}, {0, 7});
}