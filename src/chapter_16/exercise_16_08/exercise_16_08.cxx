#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>
#include <deque>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class EnglishInt {
    array<string, 10> from0to9 = {
            "", "one", "two", "three", "four",
            "five", "six", "seven", "eight", "nine"
    };
    array<string, 10> from10to19 = {
            "ten", "eleven", "twelve", "thirteen", "fourteen",
            "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
    };
    array<string, 8> tenthStrings = {
            "twenty", "thirty", "forty", "fifty", "sixty",
            "seventy", "eighty", "ninety"};

    array<string, 5> thousandStrings = {
            "", "thousand", "million", "billion", "trillion"
    };

    void calculateStringBlock(int number, stringstream &result) {
        if (number > 99) {
            auto hundred = number / 100;
            result << from0to9[hundred] << " hundred";
            number = number % 100;
            if (number > 0) {
                result << ' ';
            }
        }

        if (number > 19) {
            auto tenth = number / 10;
            result << tenthStrings[tenth - 2];
            auto remainderFrom0to9 = number % 10;
            if (remainderFrom0to9 > 0) {
                result << ' ' << from0to9[remainderFrom0to9];
            }
        } else if (number > 9) {
            result << from10to19[number - 10];
        } else {
            result << from0to9[number];
        }
    }

public:

    string solve(long number) {
        if (number == 0) {
            return "zero";
        }
        stringstream result;
        deque<uint32_t> thousands;
        auto length = 0u;
        while (number > 999) {
            auto remainder = number % 1000;
            thousands.emplace_front(remainder);
            number = number / 1000;
            length++;
        }
        thousands.emplace_front(number);
        for (auto thousandRemainder: thousands) {
            if (thousandRemainder > 0) {
                if (length < thousands.size() - 1) {
                    result << ' ';
                }
                calculateStringBlock(thousandRemainder, result);

                if (length > 0) {
                    result << ' ' << thousandStrings[length--];
                }
            }

        }
        return result.str();
    }
};

void check(long number, const string &expected) {
    EnglishInt englishInt;
    auto result = englishInt.solve(number);
    cout << number << " => '" << result << "': '" << expected << "' " << (expected == result ? '!' : '?')
         << endl;
}

int main(int argc, char **argv) {
    check(0, "zero");
    check(7, "seven");
    check(17, "seventeen");
    check(107, "one hundred seven");
    check(310, "three hundred ten");
    check(12, "twelve");
    check(13, "thirteen");
    check(900, "nine hundred");
    check(222, "two hundred twenty two");
    check(1020, "one thousand twenty");
    check(22222, "twenty two thousand two hundred twenty two");
    check(1001001, "one million one thousand one");
    check(1000000, "one million");
    check(9999999999, "nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine");
}