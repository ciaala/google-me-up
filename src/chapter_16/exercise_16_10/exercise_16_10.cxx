#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
public:
    Dice() : generator((unsigned int) clock()) {
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

class LivingPeople {

public:
    void solve(const vector<pair<uint32_t, uint32_t>> &people) {
        int years[240] = {0};
        for (auto &person : people) {
            years[person.first - 1900]++;
            years[person.second - 1900 + 1]--;
        }
        for (int i = 0; i < 240; i++) {
            cout << i + 1900 << '\t' << years[i] << endl;
        }
        auto max = -1;
        pair<int, int> maxRange = {-1, -1};
        auto balance = -1;
        auto tempNegativeBalance = -1;
        pair<int, int> currentRange = {-1, -1};

        for (int i = 0; i < 240; i++) {
            if (years[i] > 0) {
                if (currentRange.first == -1) {
                    currentRange.first = i + 1900;
                    currentRange.second = i + 1900;
                    balance = years[i];
                    tempNegativeBalance = years[i];
                } else {
                    currentRange.second = i + 1900;
                    tempNegativeBalance += years[i];
                    balance = tempNegativeBalance;
                }
                if (max < balance) {
                    max = balance;
                    maxRange = currentRange;
                }
            } else {
                if (tempNegativeBalance + years[i] < 0) {
                    currentRange = {-1, -1};
                    balance = -1;
                    tempNegativeBalance = -1;
                } else {
                    tempNegativeBalance += years[0];
                }
            }
        }
        cout << max << '[' << maxRange.first << '-' << maxRange.second << ']' << endl;
    }
};

void
generateLivingPeople(vector<pair<uint32_t, uint32_t>> &people) {
    people.reserve(400);
    Dice dice;
    for (int i = 0; i < 800; ++i) {
        auto born = dice.next(1900, 2010);
        people.emplace_back(born, born + dice.next(0, 74));
    }
}

int main(int argc, char **argv) {
    vector<pair<uint32_t, uint32_t>> people;
    generateLivingPeople(people);
    LivingPeople livingPeople;
    livingPeople.solve(people);
}