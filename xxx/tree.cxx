// Francesco

#include <iostream>

using namespace std;

template<typename ValueType>
class AvlTree;

template<typename ValueType>
ostream &operator<<( ostream &out, const AvlTree<ValueType> &tree) {
    out << tree.root->getValue();
    return out;
}
template<typename ValueType>
class AvlTree {
    class BinaryTreeNode {
    private:
        BinaryTreeNode *left;
        BinaryTreeNode *right;
        ValueType value;

    public:
        BinaryTreeNode(ValueType value) : value(value) {};
        const ValueType &getValue() { return value;};
        friend AvlTree<ValueType>;
    };

    BinaryTreeNode *root;
    void insertBalance(BinaryTreeNode * father, ValueType value) {
        if ( value < father->getValue()  ) {
            if ( father->left == nullptr) {
                father->left = new BinaryTreeNode(value);
            } else {
                insertBalance(father->left, value);
            }
        } else {
            if ( father-> right == nullptr) {
                father->right = new BinaryTreeNode(value);
            } else {
                insertBalance(father->right, value);
            }
        }

    }

public:
    void insert(ValueType value) {
        if (root == nullptr) {
            root = new BinaryTreeNode(value);
            return;
        } else {
            insertBalance(root, value);
        }
    };

    friend  ostream &operator<< <>( ostream &out, const AvlTree<ValueType> &tree);

};


int main(int argc, char **argv) {
    AvlTree<uint32_t> tree;
    tree.insert(1);
    tree.insert(2);
    tree.insert(3);
    tree.insert(4);
    tree.insert(5);
    cout << tree << endl;
}
