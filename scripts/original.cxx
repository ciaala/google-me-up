#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
#include <sstream>
#include <array>
#include <stack>
#include <queue>

using namespace std;

class Dice {
    std::default_random_engine generator;
    unsigned int seed;
public:
    Dice() : Dice((unsigned int) clock()) {

    }

    explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
        cout << "Seed: " << seed << endl;
    }

    uint32_t next(uint32_t base, uint32_t maximum) {
        std::uniform_int_distribution<uint32_t> distribution(base, maximum);
        return distribution(generator);
    }
};

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &result) {
    out << "[ ";
    for_each(result.begin(), result.end(), [&out](auto &e) { out << e << ' '; });
    out << ']';
    return out;
}

template<typename T, typename Y>
ostream &operator<<(ostream &out, const pair<T, Y> &p) {
    out << '{' << p.first << ',' << p.second << '}';
    return out;
}

int main(int argc, char **argv) {

}