#!/usr/bin/env bash
if [[ -z "$1" ]] || [[ -z "$2" ]]; then
    echo "Chapter ?"
    read chapter
    echo "Exercise ?"
    read exercise
else
    chapter=$1
    exercise=$2
fi

LOCAL_SED=sed
if [[ ${OSTYPE} == 'darwin'*  ]]; then
    echo "macOS recognized: using gsed"
    LOCAL_SED=gsed
fi
EXERCISE_ID="exercise_${chapter}_${exercise}"
BASE="../src/chapter_${chapter}/${EXERCISE_ID}/"
mkdir -p ${BASE}
cp original.cxx ${BASE}/${EXERCISE_ID}.cxx
cp original.adoc ${BASE}/README.adoc
echo "'s/XX_EXERCISE/${chapter}\\.${exercise}/g'"
${LOCAL_SED} -i "s/XX_EXERCISE/${chapter}\\.${exercise}/g" ${BASE}/README.adoc
git add ${BASE}/${EXERCISE_ID}.cxx ${BASE}/README.adoc

# Updating CMAKE FILE
CMAKE_FILE="../src/chapter_${chapter}/CMakeLists.txt"
if [[ -f ${CMAKE_FILE} ]]; then
    echo Creating file ${CMAKE_FILE}
    touch ${CMAKE_FILE}
    git add ${CMAKE_FILE}
fi
echo -e "
add_executable(${EXERCISE_ID}
    ${EXERCISE_ID}/${EXERCISE_ID}.cxx
)" >> ${CMAKE_FILE}

