//
// Created by crypt on 18/12/18.
//

#include <vector>
#include <iostream>
#include <stack>
#include <numeric>
using namespace std;

class CalculateHeight {

 public:
  /**
  * 4 10 8 6 7 9 5 11
  * 0  1 0 0 1 3 0 7
  * @param data
  * @return
  */
  vector<uint32_t> solve(const vector<uint32_t> &data) {
    vector<uint32_t> result;
    if (data.empty()) {
      return result;
    }
    result.emplace_back(0);
    if (data.size() == 1) {
      return result;
    }
    stack<uint32_t> partials;
    //result.reserve(data.size());
    result.resize(data.size());
    for (auto i = data.size()-1; i >= 1; --i) {
      uint32_t value = data[i];
      while (!partials.empty() && data[partials.top()] < value) {
        result[partials.top()] = partials.top() - i-1;
        partials.pop();
      }
      partials.push(i);
    }
    while (!partials.empty()) {
      result[partials.top()] = partials.top();
      partials.pop();
    }
    return result;
  }

};

/*
ostream &operator<<(ostream &out, const vector<uint32_t> &data) {
  out << "[ ";
  if (!data.empty()) {
    for (auto i = 0u; i < data.size() - 1; ++i) {
      out << data[i] << ", ";
    }
  }
  out << data[data.size() - 1] << " ]";
  return out;
}
*/

ostream &operator<<(ostream &out, const vector<uint32_t> &v) {
  out << "[";
  if (!v.empty()) {
    out << accumulate(v.begin() + 1, v.end(), to_string(*v.begin()),
                      [](auto a, const auto i) { return a + ", " + to_string(i); });
  }
  return out << "]";
}

int main(int argc, char **argv) {
  CalculateHeight height;

  const vector<uint32_t> &data = {4, 10, 8, 6, 7, 9, 5, 11};
  auto result = height.solve(data);
  cout << data << endl;
  cout << result << endl;
  return 0;
}
